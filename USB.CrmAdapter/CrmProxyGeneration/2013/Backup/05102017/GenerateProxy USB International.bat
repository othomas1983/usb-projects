ECHO OFF

SET url=https://crmdev.belpark.sun.ac.za/usb-int/XrmServices/2011/Organization.svc
SET domain=belpark
SET username=Integration_usb
SET password="1nT3Gra7ionUSb"
SET namespace=StellenboschUniversity.UsbInternational.Integration.Crm
SET outputDirectory=UsbInternational

CrmSvcUtil.exe ^
/codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/serviceContextName:CrmServiceContext ^
/out:%outputDirectory%\CrmServiceProxy.cs

CrmSvcUtil.exe ^
/codewriterfilter:"Microsoft.Crm.Sdk.Samples.FilteringService, GeneratePicklistEnums" ^
/codecustomization:"Microsoft.Crm.Sdk.Samples.CodeCustomizationService, GeneratePicklistEnums" ^
/namingservice:"Microsoft.Crm.Sdk.Samples.NamingService, GeneratePicklistEnums" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/out:%outputDirectory%\OptionSets.cs
