ECHO OFF

SET url=https://crmqa365.belpark.sun.ac.za/marketing/xrmservices/2011/organization.svc
SET domain=belpark
SET username=SVC_CRM_MARK_INT
SET password=crmjuly2015
SET namespace=StellenboschUniversity.Belpark.Integration.Crm
SET outputDirectory=BelparkMarketing

CrmSvcUtil.exe ^
/codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/serviceContextName:CrmServiceContext ^
/out:%outputDirectory%\CrmServiceProxy.cs

CrmSvcUtil.exe ^
/codewriterfilter:"Microsoft.Crm.Sdk.Samples.FilteringService, GeneratePicklistEnums" ^
/codecustomization:"Microsoft.Crm.Sdk.Samples.CodeCustomizationService, GeneratePicklistEnums" ^
/namingservice:"Microsoft.Crm.Sdk.Samples.NamingService, GeneratePicklistEnums" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/out:%outputDirectory%\OptionSets.cs