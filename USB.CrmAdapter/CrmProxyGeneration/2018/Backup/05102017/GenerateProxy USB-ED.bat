ECHO OFF

SET url=https://crmdev.belpark.sun.ac.za/USB-Ed/XrmServices/2011/Organization.svc
SET domain=belpark
SET username=SVC_USBED_INTDEV
SET password="1ntergr@tion_crm"
SET namespace=StellenboschUniversity.UsbEd.Integration.Crm
SET outputDirectory=USB-ED

CrmSvcUtil.exe ^
/codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/serviceContextName:CrmServiceContext ^
/out:%outputDirectory%\CrmServiceProxy.cs

CrmSvcUtil.exe ^
/codewriterfilter:"Microsoft.Crm.Sdk.Samples.FilteringService, GeneratePicklistEnums" ^
/codecustomization:"Microsoft.Crm.Sdk.Samples.CodeCustomizationService, GeneratePicklistEnums" ^
/namingservice:"Microsoft.Crm.Sdk.Samples.NamingService, GeneratePicklistEnums" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/out:%outputDirectory%\OptionSets.cs
