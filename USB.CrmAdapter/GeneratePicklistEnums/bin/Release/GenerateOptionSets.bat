ECHO OFF

SET url=https://crm365.belpark.sun.ac.za/Marketing/XRMServices/2011/Organization.svc
SET domain=belpark
SET username=SVC_CRM_MARK_INT
SET password=crmjuly2015
SET namespace=StellenboschUniversity.Belpark.Integration.Crm
SET outputDirectory=BelparkMarketing

CrmSvcUtil.exe ^
/codewriterfilter:"Microsoft.Crm.Sdk.Samples.FilteringService, GeneratePicklistEnums" ^
/codecustomization:"Microsoft.Crm.Sdk.Samples.CodeCustomizationService, GeneratePicklistEnums" ^
/namingservice:"Microsoft.Crm.Sdk.Samples.NamingService, GeneratePicklistEnums" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/out:%outputDirectory%\OptionSets.cs
pause