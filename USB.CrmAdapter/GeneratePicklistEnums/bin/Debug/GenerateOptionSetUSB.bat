ECHO OFF

SET url=https://crm.belpark.sun.ac.za/USB/XrmServices/2011/Organization.svc
SET domain=belpark
SET username=SVC_USB_INT
SET password="Usb_Crm@2015"
SET namespace=StellenboschUniversity.Usb.Integration.Crm
SET outputDirectory=USB

CrmSvcUtil.exe ^
/codewriterfilter:"Microsoft.Crm.Sdk.Samples.FilteringService, GeneratePicklistEnums" ^
/codecustomization:"Microsoft.Crm.Sdk.Samples.CodeCustomizationService, GeneratePicklistEnums" ^
/namingservice:"Microsoft.Crm.Sdk.Samples.NamingService, GeneratePicklistEnums" ^
/url:%url% ^
/domain:%domain% ^
/username:%username% ^
/password:%password% ^
/namespace:%namespace% ^
/out:%outputDirectory%\OptionSets.cs
pause