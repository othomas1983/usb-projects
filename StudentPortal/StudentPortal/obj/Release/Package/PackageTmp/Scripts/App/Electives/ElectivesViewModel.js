﻿"use strict";

var ElectivesViewModel = function () {

    var self = this;

    // Observable properties

    self.student = ko.observable();

    self.allElectives = ko.observableArray();

    self.electiveStream1 = ko.observableArray();
    self.electiveStream2 = ko.observableArray();
    self.electiveStream3 = ko.observableArray();

    self.electiveSelectedStream1 = ko.observable();
    self.electiveSelectedStream2 = ko.observable();
    self.electiveSelectedStream3 = ko.observable();
    
    self.canSelectStream1 = ko.observable(true);
    self.canSelectStream2 = ko.observable(false);
    self.canSelectStream3 = ko.observable(false);
    self.canSaveSelections = ko.observable(false);

    self.isElectiveStream1ReadOnly = ko.observable(false);
    self.isElectiveStream2ReadOnly = ko.observable(false);
    self.isElectiveStream3ReadOnly = ko.observable(false);

    //
    // Subscribers

    self.electiveSelectedStream1.subscribe(function (elective) {
        self.electiveStream2([]);
        self.canSelectStream2(false);
        self.canSaveSelections(false);

        if (elective !== undefined) {

            if (elective.ElectivesCount > 1) {
                var filteredStream2 = JSLINQ(self.allElectives()).Where(function(item) {
                    return item.DeliveryStream === 2;
                });

                self.electiveStream2(filteredStream2.items);

                self.canSelectStream2(true);
            } else {
                self.canSaveSelections(true);
            }
        }
    });

    self.electiveSelectedStream2.subscribe(function(elective) {
        self.electiveStream3([]);
        self.canSelectStream3(false);
        self.canSaveSelections(false);

        if (elective !== undefined) {

            if (elective.ElectivesCount > 2) {
                var filteredStream3 = JSLINQ(self.allElectives()).Where(function(item) {
                    return item.DeliveryStream === 3;
                });

                self.electiveStream3(filteredStream3.items);

                self.canSelectStream3(true);
            } else {
                self.canSaveSelections(true);
            }
        }
    });

    self.electiveSelectedStream3.subscribe(function (elective) {
        self.canSaveSelections(false);

        if (elective !== undefined) {
            self.canSaveSelections(true);
        }
    });

    //
    // Functions

    self.resolveStudentName = function(student) {
        if (student !== undefined) {
            return student.StudentName + " " + student.StudentSurname;
        }

        return null;
    };

    self.resolveSelectionStatus = function(status) {
        if (status !== undefined) {
            switch (status) {
            case 0:
                return "First round for elective selection";
            case 1:
                return "Second and final round for elective selection";
            case 2:

                return "Elective selection closed";
            }
        }
        return null;
    };

    self.getAllElectives = function(coHortName) {
        self.toggleLoadingOverlay("show");

        $.ajax({
            url: "Elective/GetAllModules?coHortName=" + coHortName,
            type: "GET",
            contentType: "application/json",
            success: function (data) {

                var model = JSON.parse(data);

                if (model.Errors !== undefined && model.Errors.length > 0) {
                    var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                    amplify.publish("notifications.errors", errors);
                }
                
                if (model.length > 0) {

                    self.allElectives(model);

                    var filteredStream1 = JSLINQ(model).Where(function(item) {
                        return item.DeliveryStream === 1;
                    });
                    
                    self.electiveStream1(filteredStream1.items);
                    self.getElectiveSelection();
                }

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
            },
            error: function (error) {

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                var status = error.status;
                var statusText = error.statusText;
                var responseText = error.responseText;

                alert(statusText);
            }
        });

    };

    self.getElectiveSelection = function () {
        self.toggleLoadingOverlay("show");
        self.isElectiveStream1ReadOnly(false);
        self.isElectiveStream2ReadOnly(false);
        self.isElectiveStream3ReadOnly(false);

        $.ajax({
            url: "Elective/ElectiveSelection",
            type: "GET",
            contentType: "application/json",
            success: function(data) {
                var model = JSON.parse(data);

                if (model.Errors !== undefined && model.Errors.length > 0) {
                    var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                    amplify.publish("notifications.errors", errors);
                } else {
                    
                    self.student(model);

                    if (model.StudentModules !== null && model.StudentModules.length > 0) {
                        var stream1Result = JSLINQ(model.StudentModules).Where(function(item) {
                            return item.DeliveryStream === 1;
                        });

                        if (stream1Result.items.length > 0) {
                            var matchingStream1Result = JSLINQ(self.allElectives()).Where(function(item) {
                                return item.ModuleId === stream1Result.FirstOrDefault().ModuleId &&
                                    item.CoHortName === stream1Result.FirstOrDefault().CoHortName;
                            });

                            self.electiveSelectedStream1(matchingStream1Result.FirstOrDefault());

                            if (stream1Result.items[0].SelectionStatus >= 1) {
                                self.isElectiveStream1ReadOnly(true);
                                self.canSaveSelections(false);
                            }

                        }

                        var stream2Result = JSLINQ(model.StudentModules).Where(function(item) {
                            return item.DeliveryStream === 2;
                        });

                        if (stream2Result.items.length > 0) {
                            var matchingStream2Result = JSLINQ(self.allElectives()).Where(function(item) {
                                return item.ModuleId === stream2Result.FirstOrDefault().ModuleId &&
                                    item.CoHortName === stream2Result.FirstOrDefault().CoHortName;
                            });

                            self.electiveSelectedStream2(matchingStream2Result.FirstOrDefault());

                            if (stream2Result.items[0].SelectionStatus >= 1) {
                                self.isElectiveStream2ReadOnly(true);
                                self.canSaveSelections(false);
                            }
                        }

                        var stream3Result = JSLINQ(model.StudentModules).Where(function(item) {
                            return item.DeliveryStream === 3;
                        });

                        if (stream3Result.items.length > 0) {
                            var matchingStream3Result = JSLINQ(self.allElectives()).Where(function(item) {
                                return item.ModuleId === stream3Result.FirstOrDefault().ModuleId &&
                                    item.CoHortName === stream3Result.FirstOrDefault().CoHortName;
                            });

                            self.electiveSelectedStream3(matchingStream3Result.FirstOrDefault());

                            if (stream3Result.items[0].SelectionStatus >= 1) {
                                self.isElectiveStream3ReadOnly(true);
                                self.canSaveSelections(false);
                            }
                        }
                    }
                }

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
            },
            error: function(error) {

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                var status = error.status;
                var statusText = error.statusText;
                var responseText = error.responseText;

                alert(statusText);
            }
        });

    };

    self.saveElectiveSelection = function () {

        document.getElementById("infoCallout").style.display = "none";
        document.getElementById("errorsCallout").style.display = "none";
        self.toggleLoadingOverlay("show");
        var selectedElectives = new Array();
        selectedElectives.push(self.electiveSelectedStream1());
        selectedElectives.push(self.electiveSelectedStream2());
        selectedElectives.push(self.electiveSelectedStream3());
        
        self.student().StudentModules = selectedElectives;

        $.ajax({
            url: "Elective/SaveStudentModuleSelection",
            type: "POST",
            data: JSON.stringify(self.student()),
            contentType: "application/json",
            success: function (data) {
                var model = JSON.parse(data);

                if (model.Errors !== undefined && model.Errors.length > 0) {
                    var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                    amplify.publish("notifications.errors", errors);
                }

                if (model.Information != undefined && model.Information.length > 0) {
                    var info = { Heading: model.InformationTitle, Messages: model.Information };
                    amplify.publish("notifications.info", info);
                }

                self.getElectiveSelection();
                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
            },
            error: function (error) {

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                var status = error.status;
                var statusText = error.statusText;
                var responseText = error.responseText;

                alert(statusText);
            }
        });

    };

    self.toggleLoadingOverlay = function(state) {

        switch (state) {
        case "show":
            $("#electivesSection")
                .LoadingOverlay(state,
                {
                    image: "./Content/img/avatar-red.gif",
                    size: "30%"
                });
        case "hide":
            $("#electivesSection").LoadingOverlay(state, true);
        }
    };

    self.closeInfo = function () {
        document.getElementById("selectionInfo").style.display = "none";
    };

    //
};