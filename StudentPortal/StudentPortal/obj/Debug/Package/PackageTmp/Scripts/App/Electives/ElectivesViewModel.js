﻿"use strict";

ko.validation.registerExtenders();
ko.validation.init({
    insertMessages: true,
    errorElementClass: 'has-error',
    decorateInputElement: true
}, true);

var ElectivesViewModel = function () {

    var self = this;

    // Observable properties

    self.student = ko.observable();

    self.allElectives = ko.observableArray();

    self.electiveStream1 = ko.observableArray();
    self.electiveStream2 = ko.observableArray();
    self.electiveStream3 = ko.observableArray();

    self.electiveSelectedStream1 = ko.observable().extend({ required: true });
    self.electiveSelectedStream2 = ko.observable().extend({ required: true });
    self.electiveSelectedStream3 = ko.observable().extend({ required: true });

    self.stream1StudentCount = ko.observable(0);
    self.stream2StudentCount = ko.observable(0);
    self.stream3StudentCount = ko.observable(0);

    self.isElectiveStream1ReadOnly = ko.observable(false);
    self.isElectiveStream2ReadOnly = ko.observable(false);
    self.isElectiveStream3ReadOnly = ko.observable(false);

    self.errors = ko.validation.group(self);

    //
    // Subscribers

    self.electiveSelectedStream1.extend({
        validation: {
            validator: function () {
                return self.stream1StudentCount() > 0;
            },
            message: "This module is fully booked."
        }
    });

    self.electiveSelectedStream2.extend({
        validation: {
            validator: function () {
                return self.stream2StudentCount() > 0;
            },
            message: "This module is fully booked."
        }
    });

    self.electiveSelectedStream3.extend({
        validation: {
            validator: function () {
                return self.stream3StudentCount() > 0;
            },
            message: "This module is fully booked."
        }
    });

    self.electiveSelectedStream1.subscribe(function(item) {
        self.stream1StudentCount(0);

        if (item != undefined) {
            var seatsAvailable = self.getStudentCountForStream(item);

            if (seatsAvailable != undefined) {
                var spanEl = document.getElementById("electiveSelectedStream1");

                if (seatsAvailable <= 0) {
                    spanEl.style.color = "red";
                } else {
                    spanEl.style.color = "#419641";
                }

                self.stream1StudentCount(seatsAvailable < 0 ? 0 : seatsAvailable);
            }
        }
    });

    self.electiveSelectedStream2.subscribe(function(item) {
        self.stream2StudentCount(0);

        if (item != undefined) {
            var seatsAvailable = self.getStudentCountForStream(item);

            if (seatsAvailable != undefined) {
                var spanEl = document.getElementById("electiveSelectedStream2");

                if (seatsAvailable <= 0) {
                    spanEl.style.color = "red";
                } else {
                    spanEl.style.color = "#419641";
                }

                self.stream2StudentCount(seatsAvailable < 0 ? 0 : seatsAvailable);
            }
        }
    });

    self.electiveSelectedStream3.subscribe(function(item) {
        self.stream3StudentCount(0);

        if (item != undefined) {
            var seatsAvailable = self.getStudentCountForStream(item);

            if (seatsAvailable != undefined) {
                var spanEl = document.getElementById("electiveSelectedStream3");

                if (seatsAvailable <= 0) {
                    spanEl.style.color = "red";
                } else {
                    spanEl.style.color = "#419641";
                }

                self.stream3StudentCount(seatsAvailable < 0 ? 0 : seatsAvailable);
            }
        }
    });

    //
    // Functions

    self.resolveStudentName = function(student) {
        if (student !== undefined) {
            return student.StudentName + " " + student.StudentSurname;
        }

        return null;
    };

    self.resolveSelectionStatus = function(status) {
        if (status !== undefined) {
            if (status > 1) {
                return "Elective selection closed.";
            }
            //switch (status) {
            //case 0:
            //    return "First round for elective selection";
            //case 1:
            //    return "Second and final round for elective selection";
            //case 2:

            //    return "Elective selection closed";
            //}
        }
        return null;
    };

    self.getAllElectives = function(coHortName) {
        self.toggleLoadingOverlay("show");

        $.ajax({
            url: "Elective/GetAllModules?coHortName=" + coHortName,
            type: "GET",
            contentType: "application/json",
            success: function (data) {

                var model = JSON.parse(data);

                if (model.Errors !== undefined && model.Errors.length > 0) {
                    var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                    amplify.publish("notifications.errors", errors);
                }
                
                if (model.length > 0) {

                    self.allElectives(model);

                    // Filtered Stream 1
                    var filteredStream1 = JSLINQ(model).Where(function(item) {
                        return item.DeliveryStream === 1;
                    });
                    
                    if (filteredStream1.items.length > 0) {
                        var noSelectionRequiredItem1 = JSLINQ(filteredStream1.items).Where(function(item) {
                            return item.ModuleName == "No selection required from stream 1";
                        });

                        var noSelectionRequiredItem1Index = filteredStream1.items.indexOf(noSelectionRequiredItem1.items[0]);

                        if (noSelectionRequiredItem1Index > -1) {
                            filteredStream1.items.splice(noSelectionRequiredItem1Index, 1);
                            filteredStream1.items.splice(0, 0, noSelectionRequiredItem1.items[0]);
                        }
                    }

                    self.electiveStream1(filteredStream1.items);

                    // Filtered Stream 2
                    var filteredStream2 = JSLINQ(model).Where(function (item) {
                        return item.DeliveryStream === 2;
                    });

                    if (filteredStream2.items.length > 0) {
                        var noSelectionRequiredItem2 = JSLINQ(filteredStream2.items).Where(function (item) {
                            return item.ModuleName == "No selection required from stream 2";
                        });

                        var noSelectionRequiredItem2Index = filteredStream2.items.indexOf(noSelectionRequiredItem2.items[0]);

                        if (noSelectionRequiredItem2Index > -1) {
                            filteredStream2.items.splice(noSelectionRequiredItem2Index, 1);
                            filteredStream2.items.splice(0, 0, noSelectionRequiredItem2.items[0]);
                        }
                    }

                    self.electiveStream2(filteredStream2.items);

                    // Filtered Stream 3
                    var filteredStream3 = JSLINQ(model).Where(function (item) {
                        return item.DeliveryStream === 3;
                    });

                    if (filteredStream3.items.length > 0) {
                        var noSelectionRequiredItem3 = JSLINQ(filteredStream3.items).Where(function (item) {
                            return item.ModuleName == "No selection required from stream 3";
                        });

                        var noSelectionRequiredItem3Index = filteredStream3.items.indexOf(noSelectionRequiredItem3.items[0]);

                        if (noSelectionRequiredItem3Index > -1) {
                            filteredStream3.items.splice(noSelectionRequiredItem3Index, 1);
                            filteredStream3.items.splice(0, 0, noSelectionRequiredItem3.items[0]);
                        }
                    }

                    self.electiveStream3(filteredStream3.items);

                    self.getElectiveSelection();
                }

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
            },
            error: function (error) {

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                var status = error.status;
                var statusText = error.statusText;
                var responseText = error.responseText;

                alert(statusText);
            }
        });

    };

    self.getElectiveSelection = function () {
        self.toggleLoadingOverlay("show");
        self.isElectiveStream1ReadOnly(false);
        self.isElectiveStream2ReadOnly(false);
        self.isElectiveStream3ReadOnly(false);

        $.ajax({
            url: "Elective/ElectiveSelection",
            type: "GET",
            contentType: "application/json",
            success: function(data) {
                var model = JSON.parse(data);

                if (model.Errors !== undefined && model.Errors.length > 0) {
                    var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                    amplify.publish("notifications.errors", errors);
                } else {
                    self.student(model);

                    if (model.StudentModules !== null && model.StudentModules.length > 0) {
                        var stream1Result = JSLINQ(model.StudentModules).Where(function(item) {
                            return item.DeliveryStream === 1;
                        });

                        if (stream1Result.items.length > 0) {
                            var matchingStream1Result = JSLINQ(self.allElectives()).Where(function(item) {
                                return item.ModuleId === stream1Result.FirstOrDefault().ModuleId &&
                                    item.CoHortName === stream1Result.FirstOrDefault().CoHortName;
                            });

                            self.electiveSelectedStream1(matchingStream1Result.FirstOrDefault());

                            if (stream1Result.items[0].SelectionStatus > 1) {
                                self.isElectiveStream1ReadOnly(true);
                                //self.canSaveSelections(false);
                            }

                        }

                        var stream2Result = JSLINQ(model.StudentModules).Where(function(item) {
                            return item.DeliveryStream === 2;
                        });

                        if (stream2Result.items.length > 0) {
                            var matchingStream2Result = JSLINQ(self.allElectives()).Where(function(item) {
                                return item.ModuleId === stream2Result.FirstOrDefault().ModuleId &&
                                    item.CoHortName === stream2Result.FirstOrDefault().CoHortName;
                            });

                            self.electiveSelectedStream2(matchingStream2Result.FirstOrDefault());

                            if (stream2Result.items[0].SelectionStatus > 1) {
                                self.isElectiveStream2ReadOnly(true);
                                //self.canSaveSelections(false);
                            }
                        }

                        var stream3Result = JSLINQ(model.StudentModules).Where(function(item) {
                            return item.DeliveryStream === 3;
                        });

                        if (stream3Result.items.length > 0) {
                            var matchingStream3Result = JSLINQ(self.allElectives()).Where(function(item) {
                                return item.ModuleId === stream3Result.FirstOrDefault().ModuleId &&
                                    item.CoHortName === stream3Result.FirstOrDefault().CoHortName;
                            });

                            self.electiveSelectedStream3(matchingStream3Result.FirstOrDefault());

                            if (stream3Result.items[0].SelectionStatus > 1) {
                                self.isElectiveStream3ReadOnly(true);
                                //self.canSaveSelections(false);
                            }
                        }
                    }
                }

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
            },
            error: function(error) {

                self.toggleLoadingOverlay("hide");
                //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                var status = error.status;
                var statusText = error.statusText;
                var responseText = error.responseText;

                alert(statusText);
            }
        });

    };

    self.getStudentCountForStream = function(item) {

        var seatsAvailable = 0;

        if (item != undefined) {
            $.ajax({
                url: "Elective/GetStudentCountForStream?moduleId=" + item.ModuleId,
                async: false,
                type: "GET",
                contentType: "application/json",
                success: function(data) {
                    var model = JSON.parse(data);

                    if (model.Errors !== undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    } else {
                        seatsAvailable = model;
                    }

                    self.toggleLoadingOverlay("hide");
                    //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                },
                error: function(error) {

                    self.toggleLoadingOverlay("hide");
                    //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                    var status = error.status;
                    var statusText = error.statusText;
                    var responseText = error.responseText;

                    alert(statusText);
                }
            });
        }

        return seatsAvailable;
    };

    self.saveElectiveSelection = function() {

        if (self.errors().length === 0) {

            document.getElementById("infoCallout").style.display = "none";
            document.getElementById("errorsCallout").style.display = "none";
            self.toggleLoadingOverlay("show");

            if (self.getStudentCountForStream(self.electiveSelectedStream1()) <= 0) {
                self.stream1StudentCount(0);
                document.getElementById("electiveSelectedStream1").style.color = "red";
                return;
            }
            if (self.getStudentCountForStream(self.electiveSelectedStream2()) <= 0) {
                self.stream2StudentCount(0);
                document.getElementById("electiveSelectedStream2").style.color = "red";
                return;
            }
            if (self.getStudentCountForStream(self.electiveSelectedStream3()) <= 0) {
                self.stream3StudentCount(0);
                document.getElementById("electiveSelectedStream3").style.color = "red";
                return;
            }

            var selectedElectives = new Array();
            selectedElectives.push(self.electiveSelectedStream1());
            selectedElectives.push(self.electiveSelectedStream2());
            selectedElectives.push(self.electiveSelectedStream3());

            self.student().StudentModules = selectedElectives;

            $.ajax({
                url: "Elective/SaveStudentModuleSelection",
                type: "POST",
                data: JSON.stringify(self.student()),
                contentType: "application/json",
                success: function(data) {
                    var model = JSON.parse(data);

                    if (model.Errors !== undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    }

                    if (model.Information != undefined && model.Information.length > 0) {
                        var info = { Heading: model.InformationTitle, Messages: model.Information };
                        amplify.publish("notifications.info", info);
                    }

                    self.getElectiveSelection();
                    self.toggleLoadingOverlay("hide");
                    //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                },
                error: function(error) {

                    self.toggleLoadingOverlay("hide");
                    //amplify.publish("busyLoadingDashboard", "#dashboardSection", "hide");
                    var status = error.status;
                    var statusText = error.statusText;
                    var responseText = error.responseText;

                    alert(statusText);
                }
            });

        }

    };

    self.toggleLoadingOverlay = function(state) {

        switch (state) {
        case "show":
            $("#electivesSection")
                .LoadingOverlay(state,
                {
                    image: "./Content/img/avatar-red.gif",
                    size: "30%"
                });
        case "hide":
            $("#electivesSection").LoadingOverlay(state, true);
        }
    };

    self.closeInfo = function () {
        document.getElementById("selectionInfo").style.display = "none";
    };

    //
};