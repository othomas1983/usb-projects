﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingFormUserControl.ascx.cs"
    Inherits="BellaVistaBookingForm.BookingForm.BookingFormUserControl" %>
<style type="text/css">
    
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />

<style type="text/css">
    
     .error
    {
        color: red;
    }
    #commentForm
    {
        width: 500px;
    }
    #commentForm label
    {
        width: 250px;
    }
    #commentForm label.error, #commentForm input.submit
    {
        margin-left: 253px;
    }
    #signupForm
    {
        width: 670px;
    }
    #signupForm label.error
    {
        margin-left: 10px;
        width: auto;
        display: inline;
    }
    #newsletter_topics label.error
    {
        display: none;
        margin-left: 103px;
    }
    
    
    .main-table{border-spacing: 45px 15px !important;}
    .book-heading {	
        color: #A90A36;
        font-size: 18px;
        text-transform: uppercase;
        border-bottom: 1px solid;
        padding-bottom: 5px;
    }
	
    .rightContentWrapper {
        float:right;
        width: 690px;
        padding: 0 15px 0 0px;
    }
    .booking-buttons{float:right;}
    .btnSubmit,.btnCancel{
        color: #fff;font-weight: 800;background-color: #A90A36;padding: 7px;cursor: pointer;font-size:14px
    }
    .label-td {
	line-height: 50px;
    }

</style>

<script>
    $(document).ready(function () {



    ExecuteOrDelayUntilScriptLoaded(function(){
        var InEditMode = SP.Ribbon.PageState.Handlers.isInEditMode();

        if(!InEditMode){
            
               $("#aspnetForm").validate({
                    rules: {
                        <%=txtName.UniqueID %>: {
                            minlength: 2,
                            required: true
                        },
                        <%=txtEmail.UniqueID %>: {
                            required: true,
                            email: true
                        },
                        <%=txtSurname.UniqueID %>: {
                            required: true,
                            //email:true
                        },
                        <%=txtCity.UniqueID %>: {
                            required: true,
                            //email:true
                        },
                        <%=txtTelephone.UniqueID %>: {
                            required: true,
                            //email:true
                        },
                        <%=txtNoOfRooms.UniqueID %>: {
                            required: true,
                            //email:true
                        },
                        <%=txtCheckInDate.UniqueID %>: {
                            required: true,
                            //email:true
                        },
                        <%=txtCheckOutDate.UniqueID %>: {
                            required: true,
                            //email:true
                        },
                        <%=chkTerms.UniqueID %>: {
                            required: true,
                            //email:true
                        }

                    },
                    messages: {
                        <%=txtName.UniqueID %>: {
                            required: "* Required Field *",
                            minlength: "* Please enter atleast 2 characters *"
                        }
                    }
                });
        }
    }, 'SP.Ribbon.js');


            
        
         var inDesignMode = document.forms[MSOWebPartPageFormName].MSOSPWebPartManager_DisplayModeName.value;
        //$('#<%=txtSurname.ClientID%>').msDropdown();
        //$('#<%=ddlTitle.ClientID%>').selectToAutocomplete();

        $("#<%=txtCheckInDate.ClientID%>").datepicker();

        $("#<%=txtCheckInDate.ClientID%>").change(function() {
            $("#<%=txtCheckInDate.ClientID%>").valid(); 
        });

       

       

        $("#<%=txtCheckOutDate.ClientID%>").datepicker();

         $("#<%=txtCheckOutDate.ClientID%>").change(function() {
            $("#<%=txtCheckOutDate.ClientID%>").valid(); 
        });

        


        var name = $("#<%=txtName.ClientID%>").val();
        var surname = $("#<%=txtSurname.ClientID%>").val();
        var city = $("#<%=txtCity.ClientID%>").val();
        var tel = $("#<%=txtTelephone.ClientID%>").val();
        var email = $("#<%=txtEmail.ClientID%>").val();
        var noOfRooms = $("#<%=txtNoOfRooms.ClientID%>").val();
        var terms = $("#<%=chkTerms.ClientID%>").text();


//        var nameID = <%=txtName.UniqueID%>;
//        var surnameID = $("#<%=txtSurname.ClientID%>").val();
//        var cityID = $("#<%=txtCity.ClientID%>").val();
//        var telID = $("#<%=txtTelephone.ClientID%>").val();
//        var emailID = $("#<%=txtEmail.ClientID%>").val();
//        var noOfRoomsID = $("#<%=txtNoOfRooms.ClientID%>").val();
//        var termsID = $("#<%=chkTerms.ClientID%>").text();

        


    });

    function Validate() {

        var isValid = $("#aspnetForm").valid();

        if(isValid){

            //return SendMail();

            return true;

        }

        return false;

    }


    function ClearControls() {
            
             $("#<%=txtCheckInDate.ClientID%>").val('');
             $("#<%=txtCheckOutDate.ClientID%>").val('');
             $("#<%=txtCity.ClientID%>").val('');
             $("#<%=txtEmail.ClientID%>").val('');
             $("#<%=txtName.ClientID%>").val('');
             $("#<%=txtNoOfAdults.ClientID%>").val('');
             $("#<%=txtNoOfChildren.ClientID%>").val('');
             $("#<%=txtNoOfRooms.ClientID%>").val('');
             $("#<%=txtSurname.ClientID%>").val('');
             $("#<%=txtTelephone.ClientID%>").val('');

             //Clear validation errors
        setTimeout(function() {
             $("#<%=txtCheckInDate.ClientID%>-error").hide();
              $("#<%=txtCheckOutDate.ClientID%>-error").hide();
             $("#<%=txtCity.ClientID%>-error").hide();
             $("#<%=txtEmail.ClientID%>-error").hide();
             $("#<%=txtName.ClientID%>-error").hide();
             $("#<%=txtNoOfAdults.ClientID%>-error").hide();
             $("#<%=txtNoOfChildren.ClientID%>-error").hide();
             $("#<%=txtNoOfRooms.ClientID%>-error").hide();
             $("#<%=txtSurname.ClientID%>-error").hide();
             $("#<%=txtTelephone.ClientID%>-error").hide();
              $("#<%=chkTerms.ClientID%>-error").hide();
        }, 100);


    }

    function SendMail(){
        var agreementChecked = $("#<%=chkTerms.ClientID%>").val();
        //alert(agreementChecked);

        if (agreementChecked == "on") {

            //get values
            var Title = $("#<%=ddlTitle.ClientID%>").val();
            var Name = $("#<%=txtName.ClientID%>").val();
            var Surname = $("#<%=txtSurname.ClientID%>").val();
            var City = $("#<%=txtCity.ClientID%>").val();
            var Country = $("#<%=ddlCountry.ClientID%>").val();
            var Telephone = $("#<%=txtTelephone.ClientID%>").val();
            var Mail = $("#<%=txtEmail.ClientID%>").val();
            var PreferredContact = $("#<%=ddlContact.ClientID%>").val();
            var CheckInDate = $("#<%=txtCheckInDate.ClientID%>").val();
            var CheckOutDate = $("#<%=txtCheckOutDate.ClientID%>").val();
            var NoOfAadults = $("#<%=txtNoOfAdults.ClientID%>").val();
            var NoOfChildren = $("#<%=txtNoOfAdults.ClientID%>").val();
            var RoomPreference = $("#<%=ddlRoomPreference.ClientID%>").val();
            var NoOfRooms = $("#<%=txtNoOfRooms.ClientID%>").val();
            var NumberofNights = ""; //<<-----------------------------------------------------<< Needs info

            var messageReception = "<p> A booking has been made with the following details:</p><br />";
            messageReception += "<table><tr><td>Title :</td><td>" + Title + "</td></tr>";
            messageReception += "<tr><td>First Name :</td><td>" + Name + "</td></tr>";
            messageReception += "<tr><td>Surname :</td><td>" + Surname + "</td></tr>";
            messageReception += "<tr><td>Country :</td><td>" + Country + "</td></tr>";
            messageReception += "<tr><td>Telephone :</td><td>" + Telephone + "</td></tr>";
            messageReception += "<tr><td>E-mail Address :</td><td>" + Mail + "</td></tr>";
            messageReception += "<tr><td>Preferred Contact :</td><td>" + PreferredContact + "</td></tr>";
            messageReception += "<tr><td>Check-in Date :</td><td>" + CheckInDate + "</td></tr>";
            messageReception += "<tr><td>Check-out Date :</td><td>" + CheckOutDate + "</td></tr>";
            messageReception += "<tr><td>Number of Adults :</td><td>" + NoOfAadults + "</td></tr>";
            messageReception += "<tr><td>Number of Children :</td><td>" + NoOfChildren + "</td></tr>";
            messageReception += "<tr><td>Room Preference :</td><td>" + RoomPreference + "</td></tr>";
            messageReception += "<tr><td>Number of Rooms :</td><td>" + NoOfRooms + "</td></tr></table><br />";

            var messageUser = "Hello, " + Title + " " + Name + " " + Surname + ".<br/><br/>";
            messageUser += "Thank you for choosing to stay with us at Bellvista Lodge.<br/><br/>";
            messageUser += "Your booking details are as follows:<br/><br/>";
            messageUser += "<table><tr><td>Arrival Date :</td><td>" + CheckInDate + "</td></tr>";
            messageUser += "<tr><td>Departure Date :</td><td>" + CheckOutDate + "</td></tr>";
            messageUser += "<tr><td>Number of Nights :</td><td>" + NumberofNights + "</td></tr>";
            messageUser += "<tr><td>Number of Adults :</td><td>" + NoOfAadults + "</td></tr>";
            messageUser += "<tr><td>Number of Children :</td><td>" + NoOfChildren + "</td></tr>";
            messageUser += "<tr><td>Number of Rooms Required :</td><td>" + NoOfRooms + "</td></tr>";
            messageUser += "<tr><td>Room Preference :</td><td>" + RoomPreference + "</td></tr></table><br />";
            messageUser += "Your booking will be confirmed, subject to availability, within 24 hours and sent to the e-mail address or mobile number provided.<br />";
            messageUser += "We look forward to the pleasure of having you as our guest.<br/>";
            messageUser += "Sincerely,<br/>";
            messageUser += "University of Stellenbosch<br/>";
            messageUser += "Bellvista Lodge<br/>";
            messageUser += "Bellville Park Campus<br/>";
            messageUser += "<table><tr><td>Tel :</td><td>+27 (0)21 918 4444</td></tr>";
            messageUser += "<tr><td>Fax :</td><td>+27 (0)21 918 4443</td></tr>";
            
//            $.ajax({
//                type: "POST",
//                async: "false",
//                url: "https://mandrillapp.com/api/1.0/messages/send.json",
//                data: {
//                    'key': 'caw6oHiJJMb6F33aVrqvnA',
//                    'message': {
//                        'from_email': 'bvista@belpark.sun.ac.za',
//                        'to': [
//                            {
//                                'email': 'ashvinb001@gmail.com',
//                                'name': 'BellaVistaLodgeBooking',
//                                'type': 'to'
//                            }
//                        ],
//                        'autotext': 'true',
//                        'subject': 'New Room Booking for ' + Title + " " + Name + " " + Surname,
//                        'html': messageReception
//                    }
//                }
//            }).done(function (response) {
//                //console.log(response); // if you're into that sorta thing
//            });


            $.ajax({
                type: "POST",
                async: "false",
                url: "https://mandrillapp.com/api/1.0/messages/send.json",
                data: {
                    'key': 'caw6oHiJJMb6F33aVrqvnA',
                    'message': {
                        'from_email': 'bvista@belpark.sun.ac.za',
                        'to': [
                            {
                                'email': Mail,
                                'name': 'BellaVistaLodgeBooking',
                                'type': 'to'
                            }
                        ],
                        'autotext': 'true',
                        'subject': 'Temporary BellaVista Lodge Booking',
                        'html': messageUser
                    }
                }
            }).done(function (response) {
                console.log(response); // if you're into that sorta thing
            });

            return true;


        }

    }

    function SendEmail(){

        var agreementChecked = $("#<%=chkTerms.ClientID%>").val();

        if (agreementChecked == "on") {

            //get values
            var Title = $("#<%=ddlTitle.ClientID%>").val();
            var Name = $("#<%=txtName.ClientID%>").val();
            var Surname = $("#<%=txtSurname.ClientID%>").val();
            var City = $("#<%=txtCity.ClientID%>").val();
            var Country = $("#<%=ddlCountry.ClientID%>").val();
            var Telephone = $("#<%=txtTelephone.ClientID%>").val();
            var Mail = $("#<%=txtEmail.ClientID%>").val();
            var PreferredContact = $("#<%=ddlContact.ClientID%>").val();
            var CheckInDate = $("#<%=txtCheckInDate.ClientID%>").val();
            var CheckOutDate = $("#<%=txtCheckOutDate.ClientID%>").val();
            var NoOfAadults = $("#<%=txtNoOfAdults.ClientID%>").val();
            var NoOfChildren = $("#<%=txtNoOfAdults.ClientID%>").val();
            var RoomPreference = $("#<%=ddlRoomPreference.ClientID%>").val();
            var NoOfRooms = $("#<%=txtNoOfRooms.ClientID%>").val();
            var NumberofNights = ""; //<<-----------------------------------------------------<< Needs info

            var messageReception = "<p> A booking has been made with the following details:</p>" + "<br/>Title: " + Title + "<br/>Name: " + Name + "<br/>Surname: " + Surname + "<br/>Country: " + Country + "<br/>Telephone: " + Telephone + "<br/>Mail: " + Mail + "<br/>PreferredContact: " + PreferredContact + "<br/>CheckInDate: " + CheckInDate + "<br/>CheckOutDate: " + CheckOutDate + "<br/>NoOfAadults: " + NoOfAadults + "<br/>NoOfChildren: " + NoOfChildren + "<br/>RoomPreference: " + RoomPreference + "<br/>NoOfRooms: " + NoOfRooms;
            var messageUser = "Dear " + Title + " " + Name + " " + Surname + "<br/><br/>" + "Thank you for choosing to stay with us at BellaVista Lodge <br/><br/> The Booking details are as follows<br/><br/>" + "<br/>ArrivalDate: " + CheckInDate + "<br/>Departure Date: " + CheckOutDate + "<br/>Number of Nights: " + NumberofNights + "<br/>Number of Adults: " + NoOfAadults + "<br/>Number of Children: " + NoOfChildren + "<br/>Number of Rooms Required: " + NoOfRooms + "<br/>Room Preference" + RoomPreference + " <br/>Your booking will be confirmed, subject to availability, within 24 hours and sent to the e- mail address or mobile number provided. <br/>We look forward to the pleasure of having you as our guest. <br/>Sincerely <br/>University of Stellenbosch <br/>Bellvista Lodge <br/>Bellville Park Campus <br/>Tel: +27 (0)21 918 4444 Fax +27(0)21 918 4444 <br/>E-mail: bvista@belpark.sun.ac.za <br/>";

//            $.ajax({
//                type: "POST",
//                async: "false",
//                url: "https://mandrillapp.com/api/1.0/messages/send.json",
//                data: {
//                    'key': 'caw6oHiJJMb6F33aVrqvnA',
//                    'message': {
//                        'from_email': 'bvista@belpark.sun.ac.za',
//                        'to': [
//                            {
//                                'email': 'ashvinb001@gmail.com',
//                                'name': 'BellaVistaLodgeBooking',
//                                'type': 'to'
//                            }
//                        ],
//                        'autotext': 'true',
//                        'subject': 'New Room Booking for ' + Title + " " + Name + " " + Surname,
//                        'html': messageReception
//                    }
//                }
//            }).done(function (response) {
//                //console.log(response); // if you're into that sorta thing
//            });


            $.ajax({
                type: "POST",
                async: "false",
                url: "https://mandrillapp.com/api/1.0/messages/send.json",
                data: {
                    'key': 'caw6oHiJJMb6F33aVrqvnA',
                    'message': {
                        'from_email': 'bvista@belpark.sun.ac.za',
                        'to': [
                            {
                                'email': Mail,
                                'name': 'BellaVistaLodgeBooking',
                                'type': 'to'
                            }
                        ],
                        'autotext': 'true',
                        'subject': 'Temporary BellaVista Lodge Booking',
                        'html': messageUser
                    }
                }
            }).done(function (response) {
                // console.log(response); // if you're into that sorta thing
            });

            return true;


        }

    }


//    function Button1_onclick() {
//        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$m$g_b691acf6_5e1d_4d4d_b02c_21744f808f3e$ctl00$btnConfirm", "", true, "", "", false, false));
//    }

</script>
<div class="rightContentWrapper">
    <h1 class="book-heading">
        BOOK ONLINE NOW</h1>
    <table width="100%" class="main-table">
        <tr>
            <td width="25%" class="label-td">
                <b>Title:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:DropDownList ID="ddlTitle" runat="server" Width="110px">
                    <asp:ListItem>Mr</asp:ListItem>
                    <asp:ListItem>Ms</asp:ListItem>
                    <asp:ListItem>Mrs</asp:ListItem>
                    <asp:ListItem>Miss</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>Name:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtName" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>Surname:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtSurname" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>City:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtCity" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>Country</b>
            </td>
            <td width="75%" class="label-dropdown">
                <asp:DropDownList ID="ddlCountry" runat="server" Width="110px">
                    <asp:ListItem Value="">Country...</asp:ListItem>
                    <asp:ListItem Value="Afganistan">Afghanistan</asp:ListItem>
                    <asp:ListItem Value="Albania">Albania</asp:ListItem>
                    <asp:ListItem Value="Algeria">Algeria</asp:ListItem>
                    <asp:ListItem Value="American Samoa">American Samoa</asp:ListItem>
                    <asp:ListItem Value="Andorra">Andorra</asp:ListItem>
                    <asp:ListItem Value="Angola">Angola</asp:ListItem>
                    <asp:ListItem Value="Anguilla">Anguilla</asp:ListItem>
                    <asp:ListItem Value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</asp:ListItem>
                    <asp:ListItem Value="Argentina">Argentina</asp:ListItem>
                    <asp:ListItem Value="Armenia">Armenia</asp:ListItem>
                    <asp:ListItem Value="Aruba">Aruba</asp:ListItem>
                    <asp:ListItem Value="Australia">Australia</asp:ListItem>
                    <asp:ListItem Value="Austria">Austria</asp:ListItem>
                    <asp:ListItem Value="Azerbaijan">Azerbaijan</asp:ListItem>
                    <asp:ListItem Value="Bahamas">Bahamas</asp:ListItem>
                    <asp:ListItem Value="Bahrain">Bahrain</asp:ListItem>
                    <asp:ListItem Value="Bangladesh">Bangladesh</asp:ListItem>
                    <asp:ListItem Value="Barbados">Barbados</asp:ListItem>
                    <asp:ListItem Value="Belarus">Belarus</asp:ListItem>
                    <asp:ListItem Value="Belgium">Belgium</asp:ListItem>
                    <asp:ListItem Value="Belize">Belize</asp:ListItem>
                    <asp:ListItem Value="Benin">Benin</asp:ListItem>
                    <asp:ListItem Value="Bermuda">Bermuda</asp:ListItem>
                    <asp:ListItem Value="Bhutan">Bhutan</asp:ListItem>
                    <asp:ListItem Value="Bolivia">Bolivia</asp:ListItem>
                    <asp:ListItem Value="Bonaire">Bonaire</asp:ListItem>
                    <asp:ListItem Value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</asp:ListItem>
                    <asp:ListItem Value="Botswana">Botswana</asp:ListItem>
                    <asp:ListItem Value="Brazil">Brazil</asp:ListItem>
                    <asp:ListItem Value="British Indian Ocean Ter">British Indian Ocean Ter</asp:ListItem>
                    <asp:ListItem Value="Brunei">Brunei</asp:ListItem>
                    <asp:ListItem Value="Bulgaria">Bulgaria</asp:ListItem>
                    <asp:ListItem Value="Burkina Faso">Burkina Faso</asp:ListItem>
                    <asp:ListItem Value="Burundi">Burundi</asp:ListItem>
                    <asp:ListItem Value="Cambodia">Cambodia</asp:ListItem>
                    <asp:ListItem Value="Cameroon">Cameroon</asp:ListItem>
                    <asp:ListItem Value="Canada">Canada</asp:ListItem>
                    <asp:ListItem Value="Canary Islands">Canary Islands</asp:ListItem>
                    <asp:ListItem Value="Cape Verde">Cape Verde</asp:ListItem>
                    <asp:ListItem Value="Cayman Islands">Cayman Islands</asp:ListItem>
                    <asp:ListItem Value="Central African Republic">Central African Republic</asp:ListItem>
                    <asp:ListItem Value="Chad">Chad</asp:ListItem>
                    <asp:ListItem Value="Channel Islands">Channel Islands</asp:ListItem>
                    <asp:ListItem Value="Chile">Chile</asp:ListItem>
                    <asp:ListItem Value="China">China</asp:ListItem>
                    <asp:ListItem Value="Christmas Island">Christmas Island</asp:ListItem>
                    <asp:ListItem Value="Cocos Island">Cocos Island</asp:ListItem>
                    <asp:ListItem Value="Colombia">Colombia</asp:ListItem>
                    <asp:ListItem Value="Comoros">Comoros</asp:ListItem>
                    <asp:ListItem Value="Congo">Congo</asp:ListItem>
                    <asp:ListItem Value="Cook Islands">Cook Islands</asp:ListItem>
                    <asp:ListItem Value="Costa Rica">Costa Rica</asp:ListItem>
                    <asp:ListItem Value="Cote DIvoire">Cote D'Ivoire</asp:ListItem>
                    <asp:ListItem Value="Croatia">Croatia</asp:ListItem>
                    <asp:ListItem Value="Cuba">Cuba</asp:ListItem>
                    <asp:ListItem Value="Curaco">Curacao</asp:ListItem>
                    <asp:ListItem Value="Cyprus">Cyprus</asp:ListItem>
                    <asp:ListItem Value="Czech Republic">Czech Republic</asp:ListItem>
                    <asp:ListItem Value="Denmark">Denmark</asp:ListItem>
                    <asp:ListItem Value="Djibouti">Djibouti</asp:ListItem>
                    <asp:ListItem Value="Dominica">Dominica</asp:ListItem>
                    <asp:ListItem Value="Dominican Republic">Dominican Republic</asp:ListItem>
                    <asp:ListItem Value="East Timor">East Timor</asp:ListItem>
                    <asp:ListItem Value="Ecuador">Ecuador</asp:ListItem>
                    <asp:ListItem Value="Egypt">Egypt</asp:ListItem>
                    <asp:ListItem Value="El Salvador">El Salvador</asp:ListItem>
                    <asp:ListItem Value="Equatorial Guinea">Equatorial Guinea</asp:ListItem>
                    <asp:ListItem Value="Eritrea">Eritrea</asp:ListItem>
                    <asp:ListItem Value="Estonia">Estonia</asp:ListItem>
                    <asp:ListItem Value="Ethiopia">Ethiopia</asp:ListItem>
                    <asp:ListItem Value="Falkland Islands">Falkland Islands</asp:ListItem>
                    <asp:ListItem Value="Faroe Islands">Faroe Islands</asp:ListItem>
                    <asp:ListItem Value="Fiji">Fiji</asp:ListItem>
                    <asp:ListItem Value="Finland">Finland</asp:ListItem>
                    <asp:ListItem Value="France">France</asp:ListItem>
                    <asp:ListItem Value="French Guiana">French Guiana</asp:ListItem>
                    <asp:ListItem Value="French Polynesia">French Polynesia</asp:ListItem>
                    <asp:ListItem Value="French Southern Ter">French Southern Ter</asp:ListItem>
                    <asp:ListItem Value="Gabon">Gabon</asp:ListItem>
                    <asp:ListItem Value="Gambia">Gambia</asp:ListItem>
                    <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
                    <asp:ListItem Value="Germany">Germany</asp:ListItem>
                    <asp:ListItem Value="Ghana">Ghana</asp:ListItem>
                    <asp:ListItem Value="Gibraltar">Gibraltar</asp:ListItem>
                    <asp:ListItem Value="Great Britain">Great Britain</asp:ListItem>
                    <asp:ListItem Value="Greece">Greece</asp:ListItem>
                    <asp:ListItem Value="Greenland">Greenland</asp:ListItem>
                    <asp:ListItem Value="Grenada">Grenada</asp:ListItem>
                    <asp:ListItem Value="Guadeloupe">Guadeloupe</asp:ListItem>
                    <asp:ListItem Value="Guam">Guam</asp:ListItem>
                    <asp:ListItem Value="Guatemala">Guatemala</asp:ListItem>
                    <asp:ListItem Value="Guinea">Guinea</asp:ListItem>
                    <asp:ListItem Value="Guyana">Guyana</asp:ListItem>
                    <asp:ListItem Value="Haiti">Haiti</asp:ListItem>
                    <asp:ListItem Value="Hawaii">Hawaii</asp:ListItem>
                    <asp:ListItem Value="Honduras">Honduras</asp:ListItem>
                    <asp:ListItem Value="Hong Kong">Hong Kong</asp:ListItem>
                    <asp:ListItem Value="Hungary">Hungary</asp:ListItem>
                    <asp:ListItem Value="Iceland">Iceland</asp:ListItem>
                    <asp:ListItem Value="India">India</asp:ListItem>
                    <asp:ListItem Value="Indonesia">Indonesia</asp:ListItem>
                    <asp:ListItem Value="Iran">Iran</asp:ListItem>
                    <asp:ListItem Value="Iraq">Iraq</asp:ListItem>
                    <asp:ListItem Value="Ireland">Ireland</asp:ListItem>
                    <asp:ListItem Value="Isle of Man">Isle of Man</asp:ListItem>
                    <asp:ListItem Value="Israel">Israel</asp:ListItem>
                    <asp:ListItem Value="Italy">Italy</asp:ListItem>
                    <asp:ListItem Value="Jamaica">Jamaica</asp:ListItem>
                    <asp:ListItem Value="Japan">Japan</asp:ListItem>
                    <asp:ListItem Value="Jordan">Jordan</asp:ListItem>
                    <asp:ListItem Value="Kazakhstan">Kazakhstan</asp:ListItem>
                    <asp:ListItem Value="Kenya">Kenya</asp:ListItem>
                    <asp:ListItem Value="Kiribati">Kiribati</asp:ListItem>
                    <asp:ListItem Value="Korea North">Korea North</asp:ListItem>
                    <asp:ListItem Value="Korea Sout">Korea South</asp:ListItem>
                    <asp:ListItem Value="Kuwait">Kuwait</asp:ListItem>
                    <asp:ListItem Value="Kyrgyzstan">Kyrgyzstan</asp:ListItem>
                    <asp:ListItem Value="Laos">Laos</asp:ListItem>
                    <asp:ListItem Value="Latvia">Latvia</asp:ListItem>
                    <asp:ListItem Value="Lebanon">Lebanon</asp:ListItem>
                    <asp:ListItem Value="Lesotho">Lesotho</asp:ListItem>
                    <asp:ListItem Value="Liberia">Liberia</asp:ListItem>
                    <asp:ListItem Value="Libya">Libya</asp:ListItem>
                    <asp:ListItem Value="Liechtenstein">Liechtenstein</asp:ListItem>
                    <asp:ListItem Value="Lithuania">Lithuania</asp:ListItem>
                    <asp:ListItem Value="Luxembourg">Luxembourg</asp:ListItem>
                    <asp:ListItem Value="Macau">Macau</asp:ListItem>
                    <asp:ListItem Value="Macedonia">Macedonia</asp:ListItem>
                    <asp:ListItem Value="Madagascar">Madagascar</asp:ListItem>
                    <asp:ListItem Value="Malaysia">Malaysia</asp:ListItem>
                    <asp:ListItem Value="Malawi">Malawi</asp:ListItem>
                    <asp:ListItem Value="Maldives">Maldives</asp:ListItem>
                    <asp:ListItem Value="Mali">Mali</asp:ListItem>
                    <asp:ListItem Value="Malta">Malta</asp:ListItem>
                    <asp:ListItem Value="Marshall Islands">Marshall Islands</asp:ListItem>
                    <asp:ListItem Value="Martinique">Martinique</asp:ListItem>
                    <asp:ListItem Value="Mauritania">Mauritania</asp:ListItem>
                    <asp:ListItem Value="Mauritius">Mauritius</asp:ListItem>
                    <asp:ListItem Value="Mayotte">Mayotte</asp:ListItem>
                    <asp:ListItem Value="Mexico">Mexico</asp:ListItem>
                    <asp:ListItem Value="Midway Islands">Midway Islands</asp:ListItem>
                    <asp:ListItem Value="Moldova">Moldova</asp:ListItem>
                    <asp:ListItem Value="Monaco">Monaco</asp:ListItem>
                    <asp:ListItem Value="Mongolia">Mongolia</asp:ListItem>
                    <asp:ListItem Value="Montserrat">Montserrat</asp:ListItem>
                    <asp:ListItem Value="Morocco">Morocco</asp:ListItem>
                    <asp:ListItem Value="Mozambique">Mozambique</asp:ListItem>
                    <asp:ListItem Value="Myanmar">Myanmar</asp:ListItem>
                    <asp:ListItem Value="Nambia">Nambia</asp:ListItem>
                    <asp:ListItem Value="Nauru">Nauru</asp:ListItem>
                    <asp:ListItem Value="Nepal">Nepal</asp:ListItem>
                    <asp:ListItem Value="Netherland Antilles">Netherland Antilles</asp:ListItem>
                    <asp:ListItem Value="Netherlands">Netherlands (Holland, Europe)</asp:ListItem>
                    <asp:ListItem Value="Nevis">Nevis</asp:ListItem>
                    <asp:ListItem Value="New Caledonia">New Caledonia</asp:ListItem>
                    <asp:ListItem Value="New Zealand">New Zealand</asp:ListItem>
                    <asp:ListItem Value="Nicaragua">Nicaragua</asp:ListItem>
                    <asp:ListItem Value="Niger">Niger</asp:ListItem>
                    <asp:ListItem Value="Nigeria">Nigeria</asp:ListItem>
                    <asp:ListItem Value="Niue">Niue</asp:ListItem>
                    <asp:ListItem Value="Norfolk Island">Norfolk Island</asp:ListItem>
                    <asp:ListItem Value="Norway">Norway</asp:ListItem>
                    <asp:ListItem Value="Oman">Oman</asp:ListItem>
                    <asp:ListItem Value="Pakistan">Pakistan</asp:ListItem>
                    <asp:ListItem Value="Palau Island">Palau Island</asp:ListItem>
                    <asp:ListItem Value="Palestine">Palestine</asp:ListItem>
                    <asp:ListItem Value="Panama">Panama</asp:ListItem>
                    <asp:ListItem Value="Papua New Guinea">Papua New Guinea</asp:ListItem>
                    <asp:ListItem Value="Paraguay">Paraguay</asp:ListItem>
                    <asp:ListItem Value="Peru">Peru</asp:ListItem>
                    <asp:ListItem Value="Phillipines">Philippines</asp:ListItem>
                    <asp:ListItem Value="Pitcairn Island">Pitcairn Island</asp:ListItem>
                    <asp:ListItem Value="Poland">Poland</asp:ListItem>
                    <asp:ListItem Value="Portugal">Portugal</asp:ListItem>
                    <asp:ListItem Value="Puerto Rico">Puerto Rico</asp:ListItem>
                    <asp:ListItem Value="Qatar">Qatar</asp:ListItem>
                    <asp:ListItem Value="Republic of Montenegro">Republic of Montenegro</asp:ListItem>
                    <asp:ListItem Value="Republic of Serbia">Republic of Serbia</asp:ListItem>
                    <asp:ListItem Value="Reunion">Reunion</asp:ListItem>
                    <asp:ListItem Value="Romania">Romania</asp:ListItem>
                    <asp:ListItem Value="Russia">Russia</asp:ListItem>
                    <asp:ListItem Value="Rwanda">Rwanda</asp:ListItem>
                    <asp:ListItem Value="St Barthelemy">St Barthelemy</asp:ListItem>
                    <asp:ListItem Value="St Eustatius">St Eustatius</asp:ListItem>
                    <asp:ListItem Value="St Helena">St Helena</asp:ListItem>
                    <asp:ListItem Value="St Kitts-Nevis">St Kitts-Nevis</asp:ListItem>
                    <asp:ListItem Value="St Lucia">St Lucia</asp:ListItem>
                    <asp:ListItem Value="St Maarten">St Maarten</asp:ListItem>
                    <asp:ListItem Value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</asp:ListItem>
                    <asp:ListItem Value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</asp:ListItem>
                    <asp:ListItem Value="Saipan">Saipan</asp:ListItem>
                    <asp:ListItem Value="Samoa">Samoa</asp:ListItem>
                    <asp:ListItem Value="Samoa American">Samoa American</asp:ListItem>
                    <asp:ListItem Value="San Marino">San Marino</asp:ListItem>
                    <asp:ListItem Value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</asp:ListItem>
                    <asp:ListItem Value="Saudi Arabia">Saudi Arabia</asp:ListItem>
                    <asp:ListItem Value="Senegal">Senegal</asp:ListItem>
                    <asp:ListItem Value="Serbia">Serbia</asp:ListItem>
                    <asp:ListItem Value="Seychelles">Seychelles</asp:ListItem>
                    <asp:ListItem Value="Sierra Leone">Sierra Leone</asp:ListItem>
                    <asp:ListItem Value="Singapore">Singapore</asp:ListItem>
                    <asp:ListItem Value="Slovakia">Slovakia</asp:ListItem>
                    <asp:ListItem Value="Slovenia">Slovenia</asp:ListItem>
                    <asp:ListItem Value="Solomon Islands">Solomon Islands</asp:ListItem>
                    <asp:ListItem Value="Somalia">Somalia</asp:ListItem>
                    <asp:ListItem Value="South Africa">South Africa</asp:ListItem>
                    <asp:ListItem Value="Spain">Spain</asp:ListItem>
                    <asp:ListItem Value="Sri Lanka">Sri Lanka</asp:ListItem>
                    <asp:ListItem Value="Sudan">Sudan</asp:ListItem>
                    <asp:ListItem Value="Suriname">Suriname</asp:ListItem>
                    <asp:ListItem Value="Swaziland">Swaziland</asp:ListItem>
                    <asp:ListItem Value="Sweden">Sweden</asp:ListItem>
                    <asp:ListItem Value="Switzerland">Switzerland</asp:ListItem>
                    <asp:ListItem Value="Syria">Syria</asp:ListItem>
                    <asp:ListItem Value="Tahiti">Tahiti</asp:ListItem>
                    <asp:ListItem Value="Taiwan">Taiwan</asp:ListItem>
                    <asp:ListItem Value="Tajikistan">Tajikistan</asp:ListItem>
                    <asp:ListItem Value="Tanzania">Tanzania</asp:ListItem>
                    <asp:ListItem Value="Thailand">Thailand</asp:ListItem>
                    <asp:ListItem Value="Togo">Togo</asp:ListItem>
                    <asp:ListItem Value="Tokelau">Tokelau</asp:ListItem>
                    <asp:ListItem Value="Tonga">Tonga</asp:ListItem>
                    <asp:ListItem Value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</asp:ListItem>
                    <asp:ListItem Value="Tunisia">Tunisia</asp:ListItem>
                    <asp:ListItem Value="Turkey">Turkey</asp:ListItem>
                    <asp:ListItem Value="Turkmenistan">Turkmenistan</asp:ListItem>
                    <asp:ListItem Value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</asp:ListItem>
                    <asp:ListItem Value="Tuvalu">Tuvalu</asp:ListItem>
                    <asp:ListItem Value="Uganda">Uganda</asp:ListItem>
                    <asp:ListItem Value="Ukraine">Ukraine</asp:ListItem>
                    <asp:ListItem Value="United Arab Erimates">United Arab Emirates</asp:ListItem>
                    <asp:ListItem Value="United Kingdom">United Kingdom</asp:ListItem>
                    <asp:ListItem Value="United States of America">United States of America</asp:ListItem>
                    <asp:ListItem Value="Uraguay">Uruguay</asp:ListItem>
                    <asp:ListItem Value="Uzbekistan">Uzbekistan</asp:ListItem>
                    <asp:ListItem Value="Vanuatu">Vanuatu</asp:ListItem>
                    <asp:ListItem Value="Vatican City State">Vatican City State</asp:ListItem>
                    <asp:ListItem Value="Venezuela">Venezuela</asp:ListItem>
                    <asp:ListItem Value="Vietnam">Vietnam</asp:ListItem>
                    <asp:ListItem Value="Virgin Islands (Brit)">Virgin Islands (Brit)</asp:ListItem>
                    <asp:ListItem Value="Virgin Islands (USA)">Virgin Islands (USA)</asp:ListItem>
                    <asp:ListItem Value="Wake Island">Wake Island</asp:ListItem>
                    <asp:ListItem Value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</asp:ListItem>
                    <asp:ListItem Value="Yemen">Yemen</asp:ListItem>
                    <asp:ListItem Value="Zaire">Zaire</asp:ListItem>
                    <asp:ListItem Value="Zambia">Zambia</asp:ListItem>
                    <asp:ListItem Value="Zimbabwe">Zimbabwe</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>Telephone:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtTelephone" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>E-mail:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtEmail" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>Preferred contact:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:DropDownList ID="ddlContact" runat="server" Width="110px">
                    <asp:ListItem Value="Email">Email</asp:ListItem>
                    <asp:ListItem Value="Mobile">Mobile</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>Check-in:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtCheckInDate" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>Check-out:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtCheckOutDate" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>No of adults:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtNoOfAdults" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>No of children:</b>
            </td>
            <td>
                <asp:TextBox ID="txtNoOfChildren" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <b>Room preference:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:DropDownList ID="ddlRoomPreference" runat="server" Width="110px">
                    <asp:ListItem Value="Lodge Double">Lodge Double</asp:ListItem>
                    <asp:ListItem Value="Lodge Twin">Lodge Twin</asp:ListItem>
                    <asp:ListItem Value="Lodge Double">Annex Double</asp:ListItem>
                    <asp:ListItem Value="Lodge Twin">Annex Twin</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td width="25%" class="label-td">
                <b>No of rooms:</b>
            </td>
            <td width="75%" class="text-box">
                <asp:TextBox ID="txtNoOfRooms" runat="server" Width="290px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                &nbsp; &nbsp;
                <asp:Label ForeColor="Red" ID="lblErrMessage" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                &nbsp;
                <asp:CheckBox ID="chkTerms" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged"
                    Text="I agree to the booking terms and conditions." />
            </td>
        </tr>
       
        <tr>
            <td class="style2" colspan="2">
                &nbsp; <a href="http://www.bellvistalodge.co.za/Pages/Terms-and-Conditions.aspx"
                    style="color: #A90A36;" target="_blank">To access our terms and conditions, click
                    here.</a>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2" colspan="2">
                &nbsp; &nbsp;
                <asp:Label ForeColor="Green" ID="lblConfirmationMessage" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;
            </td>
            <td class="booking-buttons">
                <asp:Button ID="btnCancel" CssClass="btnCancel" runat="server" Text="Cancel" OnClientClick="ClearControls();" />
                &nbsp; &nbsp; &nbsp;
                <asp:Button ID="btnConfirm" CssClass="btnSubmit" runat="server" Text="Book Now" OnClientClick="return Validate();"
                    OnClick="btnConfirm_Click" />
            </td>
        </tr>
    </table>
    
    <br/>
    <br/>

    <div class="InfoBlock">
    <div id="ctl00_PlaceHolderMain_ctl01_label" style="display: none">
        Booking Desc
    </div>
    <div class="ms-rtestate-field" id="ctl00_PlaceHolderMain_ctl01__ControlWrapper_RichHtmlField"
        aria-labelledby="ctl00_PlaceHolderMain_ctl01_label" style="display: inline">
        <p>
            Once the booking has been received, a booking confirmation, subject to availability,
            will be issued within 24 hours in respect of each room booked and sent to the e-mail
            address or mobile number provided.
        </p>
        <p>
            Please check BellVista Lodge’s <a href="/Pages/Terms-and-Conditions.aspx" target="_blank">
                cancellation policy</a> when you wish to make any changes to your booking.            
        </p>
    </div>
</div>

</div>



