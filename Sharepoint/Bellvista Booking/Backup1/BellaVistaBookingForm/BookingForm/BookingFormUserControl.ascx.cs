﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.ComponentModel;
using System.Web.Compilation;
using BellaVistaBookingForm;
using Microsoft.SharePoint.Utilities;

namespace BellaVistaBookingForm.BookingForm
{
    public partial class BookingFormUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ////BookingForm.SampleBoolean
            //txtCheckInDate.Text = string.Empty;
            //txtCheckOutDate.Text = string.Empty;
            //txtCity.Text = string.Empty;
            //txtEmail.Text = string.Empty;
            //txtName.Text= string.Empty;
            //txtNoOfAdults.Text= string.Empty;
            //txtNoOfChildren.Text= string.Empty;
            //txtNoOfRooms.Text= string.Empty;
            //txtSurname.Text= string.Empty;
            //txtTelephone.Text= string.Empty;

            //ddlContact.SelectedIndex = 0;
            //ddlCountry.SelectedIndex = 0;
            //ddlRoomPreference.SelectedIndex = 0;
            //ddlTitle.SelectedIndex = 0;


        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (BookingForm.WebUrl == null)
                {
                    //BookingForm.WebUrl = "http://win-edvaua4r1oe:35629";
                    //BookingForm.WebUrl = "http://bellvistalodgedev.belpark.sun.ac.za";
                    BookingForm.WebUrl = "http://www.bellvistalodge.co.za";

                    BookingForm.ListName = "Room_booking";
                    BookingForm.EmailAddressBookingAdmin = "bvista@belpark.sun.ac.za";
                }
                if (IsValid())
                {
                    using (SPSite oSite = new SPSite(BookingForm.WebUrl))
                    {
                        using (SPWeb oWeb = oSite.OpenWeb())
                        {
                            SPList list = oWeb.Lists.TryGetList(BookingForm.ListName);
                            SPListItemCollection listItems = oWeb.Lists[BookingForm.ListName].Items;
                            SPListItem item = listItems.Add();

                            item["Title"] = ddlTitle.SelectedItem.Text;
                            item["Name"] = txtName.Text.Trim();
                            item["Surname"] = txtSurname.Text.Trim();
                            item["City"] = txtCity.Text.Trim();
                            item["Telephone"] = txtTelephone.Text.Trim();
                            item["E-mail"] = txtEmail.Text.Trim();
                            item["Prefered Contact"] = ddlContact.SelectedItem.Text;
                            item["Check-in"] = txtCheckInDate.Text.Trim();
                            item["Check-out"] = txtCheckOutDate.Text.Trim();
                            item["Room_preference"] = ddlRoomPreference.Text.Trim();

                            item["No_of_adults"] = "0";
                            if (!string.IsNullOrEmpty(txtNoOfAdults.Text.Trim()))
                            {
                                item["No_of_adults"] = (txtNoOfAdults.Text.Trim());
                            }

                            item["No_of_children"] = "0";
                            if (!string.IsNullOrEmpty(txtNoOfChildren.Text.Trim()))
                            {
                                item["No_of_children"] = (txtNoOfChildren.Text.Trim());
                            }

                            item["No_of_rooms"] = "0";
                            if (!string.IsNullOrEmpty(txtNoOfRooms.Text.Trim()))
                            {
                                item["No_of_rooms"] = (txtNoOfRooms.Text.Trim());
                            }


                            item["T_C"] = chkTerms.Checked;
                            item["CountryText"] = ddlCountry.Text.Trim();

                            string fullname = item["Title"] + " " + item["Name"] + " " + item["Surname"];
                            //string messageReception = "A booking has been made with the following details:<br/><br/>" +
                            //                          "<br/>Title: " + item["Title"] + "<br/>Name: " + item["Name"] +
                            //                          "<br/>Surname: " + item["Surname"] + "<br/>Country: " +
                            //                          item["CountryText"] + "<br/>Telephone: " + item["Telephone"] +
                            //                          "<br/>Mail: " + item["E-mail"] + "<br/>Preferred Contact: " +
                            //                          item["Prefered Contact"] + "<br/>Check In Date: " +
                            //                          GetDateString(item["Check-in"].ToString()) +
                            //                          "<br/>Check Out Date: " + GetDateString(item["Check-out"].ToString())+
                            //                          "<br/>No Of Aadults: " +
                            //                          item["No_of_adults"] + "<br/>No Of Children: " +
                            //                          item["No_of_children"] + "<br/>Room Preference: " +
                            //                          item["Room_preference"] + "<br/>No Of Rooms: " +
                            //                          item["No_of_rooms"];
                            string messageReception = "A booking has been made with the following details:<br />";
                            messageReception += "<table><tr><td>Title :</td><td>" + item["Title"] + "</td></tr>";
                            messageReception += "<tr><td>First Name :</td><td>" + item["Name"] + "</td></tr>";
                            messageReception += "<tr><td>Surname :</td><td>" + item["Surname"] + "</td></tr>";
                            messageReception += "<tr><td>Country :</td><td>" + item["CountryText"] + "</td></tr>";
                            messageReception += "<tr><td>Telephone :</td><td>" + item["Telephone"] + "</td></tr>";
                            messageReception += "<tr><td>E-mail Address :</td><td>" + item["E-mail"] + "</td></tr>";
                            messageReception += "<tr><td>Preferred Contact :</td><td>" + item["Prefered Contact"] + "</td></tr>";
                            messageReception += "<tr><td>Check-in Date :</td><td>" + GetDateString(item["Check-in"].ToString()) + "</td></tr>";
                            messageReception += "<tr><td>Check-out Date :</td><td>" + GetDateString(item["Check-out"].ToString()) + "</td></tr>";
                            messageReception += "<tr><td>Number of Adults :</td><td>" + item["No_of_adults"] + "</td></tr>";
                            messageReception += "<tr><td>Number of Children :</td><td>" + item["No_of_children"] + "</td></tr>";
                            messageReception += "<tr><td>Room Preference :</td><td>" + item["Room_preference"] + "</td></tr>";
                            messageReception += "<tr><td>Number of Rooms :</td><td>" + item["No_of_rooms"] + "</td></tr></table>";

                            //var messageUser = "Dear " + item["Title"] + " " + item["Name"] + " " + item["Surname"] +
                            //                  "<br/><br/>" +
                            //                  "Thank you for choosing to stay with us at BellaVista Lodge <br/><br/> The Booking details are as follows<br/><br/>" +
                            //                  "<br/>Arrival Date: " + GetDateString(item["Check-in"].ToString()) + 
                            //                  "<br/>Departure Date: " + GetDateString(item["Check-out"].ToString()) +
                            //                  "<br/>Number of Adults: " + item["No_of_adults"] +
                            //                  "<br/>Number of Children: " + item["No_of_children"] +
                            //                  "<br/>Number of Rooms Required: " + item["No_of_rooms"] +
                            //                  "<br/>Room Preference: " + item["Room_preference"] +
                            //                  "<br/><br/>Your booking will be confirmed, subject to availability, within 24 hours and sent to the e- mail address or mobile number provided. <br/><br/>We look forward to the pleasure of having you as our guest. <br/><br/>Sincerely <br/><br/>University of Stellenbosch <br/><br/>Bellvista Lodge <br/><br/>Bellville Park Campus <br/><br/>Tel: +27 (0)21 918 4444 Fax +27(0)21 918 4444 <br/><br/>E-mail: bvista@belpark.sun.ac.za <br/><br/><br/>";
                            var messageUser = "Hello, " + item["Title"] + " " + item["Name"] + " " + item["Surname"] + ".<br/><br/>";
                            messageUser += "Thank you for choosing to stay with us at Bellvista Lodge.<br/><br/>";
                            messageUser += "Your booking details are as follows:<br/><br/>";
                            messageUser += "<table><tr><td>Arrival Date :</td><td>" + GetDateString(item["Check-in"].ToString()) + "</td></tr>";
                            messageUser += "<tr><td>Departure Date :</td><td>" + GetDateString(item["Check-out"].ToString()) + "</td></tr>";
                            messageUser += "<tr><td>Number of Adults :</td><td>" + item["No_of_adults"] + "</td></tr>";
                            messageUser += "<tr><td>Number of Children :</td><td>" + item["No_of_children"] + "</td></tr>";
                            messageUser += "<tr><td>Number of Rooms Required :</td><td>" + item["No_of_rooms"] + "</td></tr>";
                            messageUser += "<tr><td>Room Preference :</td><td>" + item["Room_preference"] + "</td></tr></table><br />";
                            messageUser += "Your booking will be confirmed, subject to availability, within 24 hours and sent to the e-mail address or mobile number provided.<br />";
                            messageUser += "We look forward to the pleasure of having you as our guest.<br/>";
                            messageUser += "Sincerely,<br/>";
                            messageUser += "University of Stellenbosch<br/>";
                            messageUser += "Bellvista Lodge<br/>";
                            messageUser += "Bellville Park Campus<br/>";
                            messageUser += "<table><tr><td>Tel :</td><td>+27 (0)21 918 4444</td></tr>";
                            messageUser += "<tr><td>Fax :</td><td>+27 (0)21 918 4443</td></tr></table>";

                            item.Update();

                            WriteToSqlDatabase();

                            ClearControls();

                            SendEmailReception(fullname, messageReception);

                            SendEmailUser(messageUser, item["E-mail"].ToString());

                            lblConfirmationMessage.Text =
                                "Your provisional booking has been successfully submitted. You will recieve a confirmation from us within 24 hours.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = ex.Message.ToString();
                //throw;
            }
        }

        private string GetDateString(string date)
        {

            return Convert.ToDateTime(date).ToShortDateString();
        }


        public void WriteToSqlDatabase()
        {
            string Title = ddlTitle.SelectedItem.Text;
            string Name = txtName.Text.Trim();
            string Surname = txtSurname.Text.Trim();
            string City = txtCity.Text.Trim();
            string Telephone = txtTelephone.Text.Trim();
            string Email = txtEmail.Text.Trim();
            string PreferedContact = ddlContact.SelectedItem.Text;
            DateTime Checkin = Convert.ToDateTime(txtCheckInDate.Text.Trim());
            DateTime Checkout = Convert.ToDateTime(txtCheckOutDate.Text.Trim());

            int No_of_adults = 0;
            if (!string.IsNullOrEmpty(txtNoOfAdults.Text.Trim()))
            {
                No_of_adults = Convert.ToInt32(txtNoOfAdults.Text.Trim());
            }

            int No_of_children = 0;
            if (!string.IsNullOrEmpty(txtNoOfChildren.Text.Trim()))
            {
                No_of_children = Convert.ToInt32(txtNoOfChildren.Text.Trim());
            }

            int No_of_rooms = 0;
            if (!string.IsNullOrEmpty(txtNoOfRooms.Text.Trim()))
            {
                No_of_rooms = Convert.ToInt32(txtNoOfRooms.Text.Trim());
            }

            string Room_preference = ddlRoomPreference.Text.Trim();
            bool T_C = chkTerms.Checked;
            string CountryText = ddlCountry.Text.Trim();

            if (string.IsNullOrEmpty(BookingForm.ConnectionString))
            {
                BookingForm.ConnectionString =
                    "Data Source=146.232.99.248;Initial Catalog=Bookings;Persist Security Info=True;User ID=BellaVistaBooking;Password=Airborne!@";
            }

            BellaVistaDataContext dc = new BellaVistaDataContext(BookingForm.ConnectionString);

            LodgeRoomBooking booking = new LodgeRoomBooking();
            booking.check_in = Checkin;
            booking.check_out = Checkout;
            booking.city = City;
            booking.country = CountryText;
            booking.date_of_booking = DateTime.Now;
            booking.email = Email;
            booking.name = Name;
            booking.no_of_adults = No_of_adults;
            booking.no_of_children = No_of_children;
            booking.no_of_rooms = No_of_rooms;
            booking.prefered_contact = PreferedContact;
            booking.room_preference = Room_preference;
            booking.surname = Surname;
            booking.telephone = Telephone;
            booking.title = Title;

            dc.LodgeRoomBookings.InsertOnSubmit(booking);

            dc.SubmitChanges();
        }

        public bool IsValid()
        {
            return true;
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void ClearControls()
        {
            txtCheckInDate.Text = string.Empty;
            txtCheckOutDate.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtName.Text = string.Empty;
            txtNoOfAdults.Text = string.Empty;
            txtNoOfChildren.Text = string.Empty;
            txtNoOfRooms.Text = string.Empty;
            txtSurname.Text = string.Empty;
            txtTelephone.Text = string.Empty;
        }

        private Boolean SendEmailReception(string fullName, string message)
        {
            try
            {
                bool flag = false;
                SPSecurity.RunWithElevatedPrivileges(
                    delegate()
                    {
                        using (SPSite site = new SPSite(
                            SPContext.Current.Site.ID,
                            SPContext.Current.Site.Zone))
                        {
                            using (SPWeb web = site.OpenWeb(SPContext.Current.Web.ID))
                            {
                                flag = SPUtility.SendEmail(web, true, true, BookingForm.EmailAddressBookingAdmin,
                                    "New Booking for " + fullName,
                                    message);
                            }
                        }
                    });
                return flag;
            }
            catch (System.Exception exp)
            {
                // Do some error logging
                return false;
            }
        }

        private Boolean SendEmailUser(string message, string emailAddress)
        {
            try
            {
                bool flag = false;
                SPSecurity.RunWithElevatedPrivileges(
                    delegate()
                    {
                        using (SPSite site = new SPSite(
                            SPContext.Current.Site.ID,
                            SPContext.Current.Site.Zone))
                        {
                            using (SPWeb web = site.OpenWeb(SPContext.Current.Web.ID))
                            {
                                flag = SPUtility.SendEmail(web, true, true, emailAddress,
                                    "Booking for Bellvista Lodge",
                                    message);
                            }
                        }
                    });
                return flag;
            }
            catch (System.Exception exp)
            {
                // Do some error logging
                return false;
            }
        }
    }
}
