﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace BellaVistaBookingForm.BookingForm
{
    [ToolboxItemAttribute(false)]
    public class BookingForm : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/BellaVistaBookingForm/BookingForm/BookingFormUserControl.ascx";

        protected override void CreateChildControls()
        {
            Control control = Page.LoadControl(_ascxPath);
            Controls.Add(control);
        }

        //public string Test;

        //public static Boolean SampleBoolean;
        //[Category("Extended Settings"),
        //Personalizable(PersonalizationScope.Shared),
        //WebBrowsable(true),
        //WebDisplayName("Sample Boolean"),
        //WebDescription("Please Choose a Sample Boolean")]
        //public Boolean _SampleBoolean
        //{
        //    get { return SampleBoolean; }
        //    set { SampleBoolean = value; }
        //}

        public static string WebUrl;
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("WebUrl"),
        WebDescription("Please Choose a Web URL")]
        public string _WebUrl
        {
            get { return WebUrl; }
            set { WebUrl = value; }
        }

        public static string ListName;
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("List Name"),
        WebDescription("Please Choose a List Name")]
        public string _ListName
        {
            get { return ListName; }
            set { ListName = value; }
        }


        public static string ConnectionString;
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Connection String Name"),
        WebDescription("Please Choose a List Name")]
        public string _ConnectionString
        {
            get { return ConnectionString; }
            set { ConnectionString = value; }
        }

        public static string EmailAddressBookingAdmin;
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Email Address BookingAdmin"),
        WebDescription("EmailAddressBookingAdmin")]
        public string _EmailAddressBookingAdmin
        {
            get { return EmailAddressBookingAdmin; }
            set { EmailAddressBookingAdmin = value; }
        }



    }
}
