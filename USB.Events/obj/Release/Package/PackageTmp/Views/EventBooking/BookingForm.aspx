﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/site.Master" Inherits="System.Web.Mvc.ViewPage<USB.Domain.Models.Event>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    BookingForm
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery") %>
    <%: System.Web.Optimization.Styles.Render("~/bundles/style") %>

    <%:Html.HiddenFor(m => m.Id) %>
    <%:Html.HiddenFor(m => m.CampaignId) %>
    <%:Html.HiddenFor(m => m.CanRegister) %>
    <%:Html.HiddenFor(m => m.Catering) %>
    <%:Html.HiddenFor(m => m.Cocktail) %>
    <%:Html.HiddenFor(m => m.EventExists) %>
    <%:Html.HiddenFor(m => m.PartnerFunction) %>
    <%:Html.HiddenFor(m => m.EventName) %>
    <%:Html.HiddenFor(m => m.EventLocation) %>
    <%:Html.HiddenFor(m => m.EventStartDateTime) %>
    <%:Html.HiddenFor(m => m.IsPayedEvent) %>
    <%:Html.HiddenFor(m => m.ApplicationCode) %>
    <%:Html.HiddenFor(m => m.EPayUrl) %>
    <%:Html.HiddenFor(m => m.Cost) %>

    <% int workshopRowCheck = 0; %>
    <% int presentationRowCheck = 0; %>

    <div class="container">
        <%--Event Heading--%>
        <div class="col-md-12">
            <div class="col-md-12" style="color: #891536; font-size: 20px; text-align: center; margin-top: 10px;">
                <%: Model.EventName.IsNotNullOrEmpty() ? Model.EventName : "N/A" %>
            </div>
        </div>
        <%--  --%>
        
        <%--Contact Details--%>
        <div class="col-md-12" style="margin-top: 40px;">
            <%: Html.ValidationSummary() %>
            <div class="col-md-2"></div>
            
            <div class="form-horizontal col-md-8">
                <article>
                    <h2 style="font-size: 10pt; color: #891536">CONTACT DETAILS</h2>

                    <div class="form-group required-Ast">
                        <label class="col-md-3 control-label" for="surname">Surname</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.Surname, new { id="surname", required="required", @class="form-control" })%>
                        </div>
                    </div>
                    
                    <div class="form-group required-Ast">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.Name, new { id="name", required="required", @class="form-control" })%>
                        </div>
                    </div>
                    
                    <div class="form-group required-Ast">
                        <label class="col-md-3 control-label" for="title">Title</label>
                        <div class="col-md-5">
                            <%:Html.DropDownListFor(m => m.Title, (List<SelectListItem>)ViewBag.Titles,"Choose...", new { id="title", required="required", @class="form-control" })%>
                        </div>
                    </div>

                    <div class="form-group required-Ast">
                        <label class="col-md-3 control-label" for="association">Association</label>
                        <div class="col-md-5">
                            <%:Html.DropDownListFor(m => m.Association, (List<SelectListItem>)ViewBag.Associations, "Choose...", new {id="association", required="required" })%>
                        </div>
                    </div>
                    
                    <div class="form-group" id="associationOther" style="display: none">
                        <label class="col-md-3 control-label" style="font-style: italic" for="associationOther">Please specify</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.AssociationOther, new { id="associationOther", @class="form-control" })%>
                        </div>
                    </div>
                
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="organisation">Employer/Organisation</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.Organisation, new { id="organisation", @class="form-control" }) %>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="jobTitle">Job Title</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.JobTitle, new { id="jobTitle", @class="form-control" }) %>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="businessPhone">Business Phone</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.BusinessPhone, new { id="businessPhone", type="tel", @class="form-control", @style="width: 208px;" }) %>                    
                            <span id="bPhoneError" style="color:red;display:none;">Invalid business number for selected country.</span>
                        </div>
                    </div>

                    <div class="form-group required-Ast">
                        <label class="col-md-3 control-label" for="mobilePhone">Mobile Phone</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.MobilePhone, new { id="mobilePhone", type="tel", required="required", @class="form-control", @style="width: 208px;" })%>
                            <span id="mPhoneError" style="color:red;display:none;">Invalid number for selected country.</span>
                        </div>
                    </div>

                    <div class="form-group required-Ast">
                        <label class="col-md-3 control-label" for="email">E-mail</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.Email, new { id="email", required="required", @class="form-control" })%>
                        </div>
                    </div>

                    <div class="form-group required-Ast">
                        <label class="col-md-3 control-label" for="country">Country</label>
                        <div class="col-md-5">
                            <%:Html.DropDownListFor(m => m.Country, (List<SelectListItem>)ViewBag.Country, "Choose...", new { required="required", id="country", @class="form-control" })%>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="province">Province</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.Province, new { id="province", @class="form-control" }) %>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="city">City</label>
                        <div class="col-md-5">
                            <%:Html.TextBoxFor(m => m.City, new { id="city", @class="form-control" }) %>
                        </div>
                    </div>

                    <div>
                        <div class="form-group">
                            <% if (Model.Cocktail)
                            {%>
                            <div class="col-md-8 attend-checkbox">
                                <label class="control-label" for="attendCocktail">Will you be attending the networking lunch @ 13:15?</label>
                                <%:Html.CheckBoxFor(m => m.AttendCocktail, new { id="attendCocktail", @checked = "checked" })%>
                            </div>
                        <% } %>
                        </div>
                        
                        <div class="form-group">
                            <% if (Model.PartnerFunction)
                            { %>
                            <div class="col-md-8">
                                <label class="control-label" for="partnerAttending">Attending with a partner?</label>
                                <%:Html.CheckBoxFor(m => m.PartnerAttending, new { id="partnerAttending", @checked = "checked" })%>
                            </div>
                            <% } %>
                        </div>
                        
                        <div>
                            
                            <% if (Model.Catering)
                                { %>
                            <article>
                                <h2 style="font-size: 10pt; color: #891536">Catering</h2>
                                
                                <div class="form-group required-Ast">
                                    <label class="col-md-3 control-label" for="dietaryRequirements">My Dietary requirements</label>
                                    <div class="col-md-5">
                                        <%:Html.DropDownListFor(m => m.DietaryRequirements, (List<SelectListItem>)ViewBag.DietaryRequirements, new { id="dietaryRequirements", required="required", @class="form-control" })%>
                                    </div>
                                </div>
                                
                                <div class="form-group" id="myOther" style="display:none;">
                                    <label class="col-md-3 control-label" for="dietaryRequirementsOther">If other, please specify your dietary requirements</label>
                                    <div class="col-md-5">
                                        <%:Html.TextBoxFor(m => m.DietaryRequirementsOther, new { id="dietaryRequirementsOther", @class="form-control" }) %>
                                    </div>
                                </div>
                                
                                <div>
                                <% if (Model.PartnerFunction)
                                    { %>
                                <div id="partner" class="form-group">
                                    <label class="col-md-3 control-label" for="partnerDietaryRequirements">Partner dietary requirements</label>
                                    <div class="col-md-5">
                                        <%:Html.DropDownListFor(m => m.PartnerDietaryRequirements, (List<SelectListItem>)ViewBag.DietaryRequirements, new { id = "partnerDietaryRequirements", @class = "form-control" })%>
                                    </div>
                                </div>
                                <div id="partnerOther" class="form-group" style="display:none;">
                                    <label class="col-md-3 control-label" for="partnerDietaryRequirementsOther">If other, please specify partner's dietary requirements</label>
                                    <div class="col-md-5">
                                        <%:Html.TextBoxFor(m => m.PartnerDietaryRequirementsOther, new { id="partnerDietaryRequirementsOther", @class="form-control" }) %>
                                    </div>
                                    <% } %>
                                </div>
                            </div>
                                </article>
                            <% } %>
                                
                            <%if (Model.EventActivities.IsNotNull())
                                { %>
                            <div class="form-group">
                                <div class="col-md-8">
                                    <label class="control-label" for="showEventDetails">Show Event Activities</label>
                                    <input id="showEventDetails" type="checkbox" />
                                </div>
                                
                            </div>

                        <div id="eventDetails">

                            <div style="margin-top: 10px;">
                                <table class="table" style="overflow-y: auto; border-color: olive; border-style: solid; border-width: thin;">
                                    <tr>
                                        <th style="background-color: olive; text-align: center; color: white;">PROGRAMME - <%: DateTime.Parse(Model.EventStartDateTime.SubstringBefore("|")).ToString("dddd, dd MMMM yyyy") %>
                                        </th>
                                    </tr>
                                    <tr style="text-align: center; background-color: darkkhaki; margin: 0 10px 0 10px;">
                                        <td>
                                            <% if (Model.EventActivities.IsNotNull())
                                                {
                                                    DateTime? eventStartTime = Model.EventActivities.Min(x => x.StartTime);

                                                    if (eventStartTime.HasValue)
                                                    { %>
                                                        REGISTER FROM <%: eventStartTime.Value.AddMinutes(-30).ToString("HH:mm") %> FOR FIRST WORKSHOP
                                                    <%}
                                                } %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="table">
                                                <tr style="border-color: darkkhaki; border-style: solid">
                                                    <th style="text-align: center; background-color: darkkhaki;" colspan="4">Workshops
                                                    </th>
                                                </tr>
                                                <tr style="margin-top: 10px;">
                                                    <th>Time</th>
                                                    <th>&#10004;</th>
                                                    <th><%:Model.EventActivities.Where(x => x.ActivityType == USB.Domain.Enums.ActivityType.Workshops).FirstOrDefault().Venue %></th>
                                                    <th>Presenters</th>
                                                </tr>
                                                <tr></tr>
                                                <% for (int i = 0; i < Model.EventActivities.Count; i++)
                                                    { %>
                                                <tr>
                                                    <% if (Model.EventActivities[i].ActivityType == USB.Domain.Enums.ActivityType.Workshops)
                                                        { %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Id) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].ActivityType) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].StartTime) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].EndTime) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Subject) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Presenter) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Venue) %>
                                                    <td>
                                                        <%: Model.EventActivities[i].StartTime.ToString("HH:mm") + " - " + Model.EventActivities[i].EndTime.ToString("HH:mm") %>
                                                    </td>
                                                    <td>
                                                        <%:Html.CheckBoxFor(a => Model.EventActivities[i].IsAttending, new { @checked = "checked", @class = "checkRow" + workshopRowCheck++ })%>
                                                    </td>
                                                    <td>
                                                        <%: Model.EventActivities[i].Subject %>
                                                    </td>
                                                    <td>
                                                        <%: Model.EventActivities[i].Presenter %>
                                                    </td>
                                                    <%} %>
                                                </tr>
                                                <% } %>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="table">
                                                <tr style="border-color: darkkhaki; border-style: solid">
                                                    <th style="text-align: center; color: olive;" colspan="4">Presentations
                                                    </th>
                                                </tr>
                                                <tr style="margin-top: 10px;">
                                                    <th>Time</th>
                                                    <th>&#10004;</th>
                                                    <th><%:Model.EventActivities.Where(x => x.ActivityType == USB.Domain.Enums.ActivityType.Presentations).FirstOrDefault().Venue %></th>
                                                    <th>Presenters</th>
                                                </tr>
                                                <tr></tr>
                                                <% for (int i = 0; i < Model.EventActivities.Count; i++)
                                                    { %>
                                                <tr>
                                                    <% if (Model.EventActivities[i].ActivityType == USB.Domain.Enums.ActivityType.Presentations)
                                                        { %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Id) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].ActivityType) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].StartTime) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].EndTime) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Subject) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Presenter) %>
                                                    <%: Html.HiddenFor(x => Model.EventActivities[i].Venue) %>
                                                    <td>
                                                        <%: Model.EventActivities[i].StartTime.ToString("HH:mm") + " - " + Model.EventActivities[i].EndTime.ToString("HH:mm") %>
                                                    </td>
                                                    <td>
                                                        <%:Html.CheckBoxFor(a => Model.EventActivities[i].IsAttending, new { @checked = "checked", @class = "checkRow" + presentationRowCheck++ })%>
                                                    </td>
                                                    <td>
                                                        <%: Model.EventActivities[i].Subject %>
                                                    </td>
                                                    <td>
                                                        <%: Model.EventActivities[i].Presenter %>
                                                    </td>
                                                    <%} %>
                                                </tr>
                                                <% } %>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <%} %>

                        <% if (Model.IsPayedEvent)
                            { %>
                        <div class="form-group" style="text-align: center;">
                            <input id="paymentGateway" type="submit" class="linkButton" value="Submit RSVP" data-toggle="modal" />
                        </div>
                        <% }
                            else
                            { %>
                        <div class="form-group" style="text-align: center;">
                            <input class="linkButton" id="submitForm" type="submit" value="Submit RSVP" />
                        </div>
                        <% } %>

                        <div class="success-message">
                            <label class="textMessage">
                                You will receive communication from the University of Stellenbosch Business School because of your association with us
                                (alumnus, student staff events participant, business associate, etc.)</label>
                        </div>
                            </div>
                </div>

                </article>
            </div>

            <div class="col-md-2"></div>
        </div>
        <%--  --%>
    </div>

    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModal-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="paymentModal-label">Event Booking</h4>
                </div>
                <div class="modal-body">
                    <p>Please note that your booking is only confirmed once you have made your payment either by credit card or EFT.</p>


                    <a id="creditCardPayment" href="#" class="btn btn-default">
                        <span class="glyphicon glyphicon-credit-card"></span>&nbsp Credit Card
                    </a>
                    <%:Html.HiddenFor(m => m.EpayReference) %>
                    <div class="margin-top-10">
                        <a id="eftPayment" href="#" class="btn btn-default">
                            <span class="glyphicon glyphicon-download-alt"></span>&nbsp EFT
                        </a>
                    </div>

                    <div id="paymentDetails" style="display: none">
                        <br />
                        <div>
                            <label>Event Cost</label>
                            <label style="color: #eb2222">R <%:(int)Math.Round(Model.Cost.GetValueOrDefault() * 100)/(double)100 %></label>
                        </div>
                        <b>Bank Details</b>
                        <br />
                        <div>
                            <label>Account Name:</label><label>US Business School</label>
                        </div>
                        <div>
                            <label>Bank:</label><label>Standard Bank</label>
                        </div>
                        <div>
                            <label>Branch Code:</label><label>050610</label>
                        </div>
                        <div>
                            <label>Account Number:</label><label>07 300 306 9</label>
                        </div>
                        <div>
                            <label>Your Ref:</label><label style="color: #eb2222" id="epayRef"></label>
                        </div>
                        <br />
                        <p style="color: #eb2222">IMPORTANT: Please use the above reference number when you make your payment and send your proof of payment to <a href="mailto:USBAlumni@usb.ac.za" target="_top">USBAlumni@usb.ac.za</a></p>
                        <br />
                        <div id="payOK" class="container">
                            <button type="button" class="btn btn-primary btn-md glyphicon glyphicon-ok">&nbsp Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {
            if (document.getElementById("showEventDetails") != undefined) {
                document.getElementById("showEventDetails").checked = true;
            }

            $("#showEventDetails").change(function () {
                if ($("#showEventDetails").prop("checked")) {
                    $("#eventDetails").show();
                } else {
                    $("#eventDetails").hide();
                }
            });

            $("input:checkbox").change(function () {
                var group = ":checkbox[class='" + $(this).attr("class") + "']";
                if ($(this).is(':checked')) {
                    $(group).not($(this)).attr("checked", false);
                }
            });

            $(document).on('invalid-form.validate', 'form', function () {
                var button = $(this).find('input[type="submit"]');
                setTimeout(function () {
                    button.removeAttr('disabled');
                }, 1);
            });
            $(document).on('submit', 'form', function () {
                var button = $(this).find('input[type="submit"]');
                setTimeout(function () {
                    button.attr('disabled', 'disabled');
                }, 0);
            });
        });  

       
        var eventBooking = new EventBooking();
        //$(document).ready(function () {
        //    eventBooking.load();
        //});
    </script>
</asp:Content>

