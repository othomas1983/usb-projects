﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/site.Master" Inherits="System.Web.Mvc.ViewPage<USB.Domain.Models.Event>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    BookingForm
</asp:Content>


    
    
        
    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
           <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery") %>
    <%: System.Web.Optimization.Styles.Render("~/bundles/style") %>
    
  
        
    
    <script type="text/javascript">
        var usbEventBooking = new USBEventBooking();
        $(document).ready(function () {
            usbEventBooking.load();
        });
    </script>  

</asp:Content>
    
    <a class="linkButton" href="<%=Url.Action("BookingForm","Home")%>">Book Here</a>

