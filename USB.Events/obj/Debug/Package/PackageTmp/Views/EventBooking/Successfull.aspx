﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <link href="../../Content/style.css" rel="stylesheet"/>
    <title>Successfull</title>
</head>
<body>
    <div class="container">
        <div class="success-container">
            <div class="returnToEvents">
                <a class="success-msg" target="_parent" href="http://www.usb.ac.za/Pages/NewsEvents/Events-calendar.aspx">Return to Events List</a>
            </div>
        </div>
    </div>
</body>
</html>
