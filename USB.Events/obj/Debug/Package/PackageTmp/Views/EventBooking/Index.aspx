﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/site.Master" Inherits="System.Web.Mvc.ViewPage<USB.Domain.Models.Event>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery") %>
    <%: System.Web.Optimization.Styles.Render("~/bundles/style") %>
    
    <div class="container">
        
        <% if (Model.CampaignId != Guid.Empty)
           { %>
        
        <script type="text/javascript" src="https://analytics-eu.clickdimensions.com/ts.js"></script>

        <script type="text/javascript">

            


            $(document).ready(function () {

                var campaignId = $("#CampaignCode").val();

                if (campaignId != undefined) {
                    document.getElementById("footer").style.visibility = "visible";

                    var cdCampaignKey = campaignId;

                    var cdAnalytics = new clickdimensions.Analytics("analytics-eu.clickdimensions.com");
                    cdAnalytics.setAccountKey("aBloFWPmnUe7eZHmdJaWog");
                    cdAnalytics.setDomain("sun.ac.za");
                    //cdAnalytics.setScore(typeof (cdScore) == "undefined" ? 0 : (cdScore == 0 ? null : cdScore));
                    cdAnalytics.trackPage();
                } else {
                    document.getElementById("footer").style.visibility = "hidden";
                }

            });

        </script>

        <%--Event Heading--%>
        <%--<div class="col-md-12" style="color: #891536; font-size: 20px; text-align: center; margin-top: 10px;">--%>
            <%--<%: Model.EventName %>--%>
        <%--</div>--%>
        <%--  --%>

        <%--Event HTML content from CRM--%>
        <div class="col-md-12" style="margin-top: 20px;">
            <%: Html.Raw(Model.Html) %>
        </div>
        <%--  --%>
        
        <%--Event hidden fields--%>
        <%: Html.HiddenFor(m => m.CampaignId) %>
        <%: Html.HiddenFor(m => m.CampaignCode) %>
        <%: Html.HiddenFor(m => m.CanRegister) %>
        <%: Html.HiddenFor(m => m.Catering) %>
        <%: Html.HiddenFor(m => m.Cocktail) %>
        <%: Html.HiddenFor(m => m.EventExists) %>
        <%: Html.HiddenFor(m => m.PartnerFunction) %>
        <%: Html.HiddenFor(m => m.EventName) %>
        <%: Html.HiddenFor(m => m.EventLocation) %>
        <%: Html.HiddenFor(m => m.EventStartDateTime) %>
        <%: Html.HiddenFor(m => m.IsPayedEvent) %>
        <%: Html.HiddenFor(m => m.ApplicationCode) %>
        <%: Html.HiddenFor(m => m.EPayUrl) %>
        <%: Html.HiddenFor(m => m.Cost) %>
        <% if (Model.EventActivities.IsNotNull())
           {
               for (int i = 0; i < Model.EventActivities.Count; i++)
               { %>
        <%: Html.HiddenFor(x => x.EventActivities[i].ActivityType) %>
        <%: Html.HiddenFor(x => x.EventActivities[i].EndTime) %>
        <%: Html.HiddenFor(x => x.EventActivities[i].Id) %>
        <%: Html.HiddenFor(x => x.EventActivities[i].IsAttending) %>
        <%: Html.HiddenFor(x => x.EventActivities[i].Presenter) %>
        <%: Html.HiddenFor(x => x.EventActivities[i].StartTime) %>
        <%: Html.HiddenFor(x => x.EventActivities[i].Subject) %>
        <%: Html.HiddenFor(x => x.EventActivities[i].Venue) %>
        <% }
           }
        %>
        <%--  --%>
        
        <div class="col-md-12" style="margin-top: 20px; text-align: center;">
            <% if (Model.CanRegister)
               { %>
            <input id="loadBookingForm" class="linkButton" type="submit" value="RSVP">
            <% }
               else
               { %>
            <fieldset  style="border-color: red; border-style: solid; border-width: thin; padding: 0;">
                <div class="col-md-12">
                    <p style="margin: 10px;">
                        <span style="color: red;">This event is now fully subscribed</span>
                    </p>
                </div>
            </fieldset>
            <% } %>
        </div>
        
        <% }
            else
            { %>
        <div class="col-md-12" style="margin-top: 40px;">
            <fieldset  style="border-color: red; border-style: solid; border-width: thin; padding: 0;">
                <div class="col-md-10">
                    <h4 style="margin: 10px; color: red;">Error</h4>
                    <p style="margin: 10px; color: red;">
                        <span>No information found for this event.</span>
                    </p>
                </div>
            </fieldset>
        </div>
            <%} %>
    </div>
</asp:Content>
