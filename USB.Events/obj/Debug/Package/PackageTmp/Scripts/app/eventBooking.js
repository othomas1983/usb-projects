﻿
function EventBooking() {

    function AssociationValidation() {
        if ($('#association option:selected').text() == "Other") {
            $('#associationOther').show();
        } else {
            $('#associationOther').hide();
        }
    }

    $('#partner').hide();
    $('#partnerOther').hide();

    $("#loadBookingForm").click(function() {
        var data = $("#frmMain").serialize();
        $.ajax({
            type: 'POST',
            data: data,
            cache: false,
            url: "USBEvents/EventBooking/BookingForm",
            error: function(e, a, r) {
                alert(e);
            }
        });

    });
    //My dietary
    $('#dietaryRequirements').change(function () {
        //If Dietary other is selected
        var myDiet = $("#dietaryRequirements option:selected").text();
     
        if (myDiet == "Other") {
          
            $('#myOther').show(500);
        }
        else {
           
            $('#myOther').hide(500);
        }
    });
    // Partner Dietary
    $('#partnerDietaryRequirements').change(function () {
        //If Dietary other is selected
        var partnerDiet = $("#partnerDietaryRequirements option:selected").text();
        if (partnerDiet =="Other") {

            $('#partnerOther').show(500);
        }
        else {

            $('#partnerOther').hide(500);
        }
    });

    $('#partnerAttending').change(function () {
        if ($(this).is(':checked')) {
            $('#partner').show(500);
            //$('#partnerOther').show(500);
        }
        else {
            $('#partner').hide(500);
            $('#partnerOther').hide(500);
        }
    });

    $('#paymentGateway').click(function() {

        //SubmitBooking
        var data = $("#frmMain").serialize();
        $.ajax({
            type: 'POST',
            data: data,
            cache: false,
            url: "USBEvents/EventBooking/SubmitBooking",
            success: function(j) {
                $('#epayRef').text(j.EpayReference);
                $('#paymentModal').modal('show');
                $('#paymentModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            },
            error: function(e, a, r) {
                alert("Please make sure all required fields are filled in.");
            }
        });
    });

    $("#creditCardPayment").click(function() {
        var payRef = $('#epayRef').text();
        var data = $("#frmMain").serialize();
        $.ajax({
            type: 'POST',
            data: data,
            cache: false,
            async: false,
            url: "USBEvents/EventBooking/CreditCardPayment?epayRef=" + payRef,
            success: function(j) {
                window.open(j.EpayURL, '_blank');

                window.location.href = "USBEvents/EventBooking/Successfull";
            },
            error: function(e, a, r) {
                alert(e);
            }
        }).always(function() {
            $('#paymentModal').modal('hide');
        });
    });


    $("#eftPayment").click(function() {
        $("#paymentDetails").show(500);
    });

    $("#payOK").click(function() {
        window.location.href = "USBEvents/EventBooking/Successfull";
    });

    $('#association').change(function() {
        AssociationValidation();
    });

    $("#mobilePhone").change(function () {

        if (this.value != undefined && this.value != "") {
            $("#mobilePhone").intlTelInput("setNumber", this.value);
            var intlNumber = $("#mobilePhone").intlTelInput("getNumber");

            $("#mobilePhone").val(intlNumber);

            if ($('#mobilePhone').attr('placeholder') != undefined && intlNumber !== "") {
                var numberLength = $('#mobilePhone').attr('placeholder').replace(/\s/g, "").length;
                if (!$("#mobilePhone").intlTelInput("isValidNumber")) {
                    //alert("Invalid number for selected country.");
                    $('#mPhoneError').show();

                }
                else
                {
                    $('#mPhoneError').hide();
                }
            }
        }

    });

    $("#mobilePhone")
        .intlTelInput({
            allowExtensions: true,
            autoHideDialCode: true,
            autoPlaceholder: true,
            nationalMode: false,
            preferredCountries: ["za"],
            utilsScript: "/USBEvents/Scripts/utils.js"
        });

    $("#mobilePhone")
        .on("countrychange",
            function (e, countryData) {
                var extension = $("#mobilePhone").intlTelInput("getExtension");
                $("#mobilePhone").intlTelInput("setNumber", "+" + extension);
            });

    $("#businessPhone").change(function () {

        if (this.value != undefined && this.value != "") {
            $("#businessPhone").intlTelInput("setNumber", this.value);
            var intlNumber = $("#businessPhone").intlTelInput("getNumber");

            $("#businessPhone").val(intlNumber);

            if ($('#businessPhone').attr('placeholder') != undefined && intlNumber !== "") {
                var numberLength = $('#businessPhone').attr('placeholder').replace(/\s/g, "").length;
                if (!$("#businessPhone").intlTelInput("isValidNumber")) {
                    $('#bPhoneError').show();

                }
                else
                {
                    $('#bPhoneError').hide();
                }
            }
        }

    });

    $("#businessPhone")
        .intlTelInput({
            allowExtensions: true,
            autoHideDialCode: true,
            autoPlaceholder: true,
            nationalMode: false,
            numberType: "FIXED_LINE",
            preferredCountries: ["za"],
            utilsScript: "/USBEvents/Scripts/utils.js"
        });

    $("#mobilePhone")
        .on("countrychange",
            function (e, countryData) {
                var extension = $("#mobilePhone").intlTelInput("getExtension");
                $("#mobilePhone").intlTelInput("setNumber", "+" + extension);
            });

    $(document).ready(function() {
        window.parent.scrollTo(0, 0);
    });
}